# frozen_string_literal: true

class CacheBox
  class Chain
    include CacheBox::Helper::Validate

    # Input:
    #
    # namespace = String | Symbol # Default: 'namespace'
    #
    # Output: N/A
    def initialize(namespace = nil, logger: nil, storage: nil)
      namespace ||= :namespace
      validate_string_or_symbol!(namespace, 'namespace')

      logger  ||= Logger.new(STDOUT, level: Logger::INFO)
      storage ||= ::CacheBox::Storage::File.new(namespace: namespace)

      @unit   = ::CacheBox::Unit.new(namespace, logger: logger, storage: storage)
      @chain  = []
      @lookup = Set.new

      @namespace = namespace
      @logger    = logger
    end

    # Input:
    #
    # name   = String | Symbol
    # input  = Object # Default: nil
    # &block = Proc(Hash{...Object => Object}, *args)
    #
    # Output: self
    def add(name, args = nil, &block)
      validate_string_or_symbol!(name, 'name')
      validate_block_presence!(block, '#add')

      name_o = name
      name_s = name.to_s
      if @lookup.include?(name_s)
        raise(
          ArgumentError,
          "Chain already contains a step named #{name_o.inspect}"
        )
      else
        @lookup << name_s
      end

      @chain.push(
        [name_o, proc { |input| @unit.with(name_s, args, input, &block) }]
      )

      self
    end

    # Output: Object # Anything the last block in the chain returns.
    def run!
      work = []

      @chain.reverse_each do |name, callable|
        work.push([name, callable])

        break if @unit.has?(name)
      end

      result   = nil
      previous = nil
      work.reverse_each do |name, callable|
        result = callable.call(result)

        @unit.clear(previous) if previous
        previous = name
      end

      result
    end

    # Input:
    #
    # name = Array[String | Symbol] # Default: []
    #
    # Output: self
    def expire!(*names)
      validate_array_of_string_or_symbol!(names, 'names')
      @unit.expire!(*names)

      self
    end
  end
end
