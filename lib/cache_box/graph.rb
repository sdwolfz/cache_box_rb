# frozen_string_literal: true

class CacheBox
  class Graph
    include CacheBox::Helper::Validate

    # Input:
    #
    # namespace = String | Symbol # Default: 'namespace'
    # scheduler = CacheBox::Scheduler::*
    # storage   = CacheBox::Storage::*
    # logger    = Logger
    #
    # Output: N/A
    def initialize(namespace = nil, scheduler: nil, storage: nil, logger: nil)
      namespace ||= :namespace
      validate_string_or_symbol!(namespace, 'namespace')

      @namespace = namespace
      @logger    = logger    || Logger.new(STDOUT, level: Logger::INFO)
      @storage   = storage   || ::CacheBox::Storage::File.new(namespace: @namespace)
      @scheduler = scheduler || ::CacheBox::Scheduler::Serial.new(logger: @logger)
      @unit      = ::CacheBox::Unit.new(@namespace, logger: @logger, storage: @storage)

      @nodes = {}
    end

    # Input:
    #
    # edge   = String |
    #          Symbol |
    #          Hash{String | Symbol => String | Symbol | Array[String | Symbol]}
    # args   = Object
    # &block = Proc(Hash{...Object => Object}, *args)
    #
    # Output: self
    def node(edge, args = nil, &block)
      validate_edge!(edge)
      validate_block_presence!(block, '#node')

      @scheduler.add(edge)
      name_o = extract_name(edge)
      name_s = name_o.to_s

      @nodes[name_s] = [
        name_o,
        proc { |input| @unit.with(name_s, args, input, &block) }
      ]

      self
    end

    # Input: N/A
    #
    # Output: Object # Anything the last block in the chain returns.
    def run!
      @scheduler.run!(@nodes, @unit)
    end

    # Input:
    #
    # name = Array[String | Symbol] # Default: []
    #
    # Output: self
    def expire!(*names)
      validate_array_of_string_or_symbol!(names, 'names')
      @unit.expire!(*names)

      self
    end

    private

    # Input:
    #
    # edge = String |
    #        Symbol |
    #        Hash{String | Symbol => String | Symbol | Array[String | Symbol]}
    #
    # Output: String
    def extract_name(edge)
      return edge.to_s if edge.is_a?(Symbol) || edge.is_a?(String)

      edge.keys.first.to_s
    end
  end
end
