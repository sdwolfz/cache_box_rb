# frozen_string_literal: true

class CacheBox
  module Scheduler
    class Base
      include CacheBox::Helper::Validate

      # Input:
      #
      # logger = Logger
      #
      # Output: N/A
      def initialize(logger: nil)
        @logger = logger || Logger.new(STDOUT, level: Logger::INFO)

        @lookup = {}
        @needs  = {}
        @feeds  = {}
        @roots  = []
        @leaves = []
      end

      # Input:
      #
      # edge = String |
      #        Symbol |
      #        Hash{String | Symbol => String | Symbol | Array[String | Symbol]}
      #
      # Output: self
      def add(edge)
        validate_edge!(edge)

        needs_o = extract_needs(edge)
        needs_s = needs_o.map(&:to_s)
        needs_s.each_with_index do |arg, index|
          next if @lookup.key?(arg)

          raise(
            ArgumentError,
            "Graph does not contain required node #{needs_o[index].inspect}"
          )
        end

        name_o = extract_name(edge)
        name_s = name_o.to_s
        if @lookup.key?(name_s)
          raise(
            ArgumentError,
            "Graph already contains a node named #{name_o.inspect}"
          )
        end

        @lookup[name_s] = name_o
        @needs[name_s]  = needs_s

        @leaves << name_s
        needs_s.each do |need_s|
          existing = @feeds[need_s]

          @feeds[need_s] = existing ? existing << name_s : [name_s]
          @leaves.delete(need_s)
        end
        @roots << name_s if needs_s == []

        self
      end

      # Input:
      #
      # nodes = Hash{...String => Array[String | Symbol, Proc(Object)]}
      # unit  = CacheBox::Unit
      #
      # Output: Hash{...String | Symbol => Object}
      def run!(_nodes, _unit)
        raise(
          NotImplementedError,
          'The `#run/2` method needs to be implemented in your subclass!'
        )
      end

      private

      # Input: String | Symbol | Hash[1 String | Symbol => Object]
      #
      # Output: String | Symbol
      def extract_name(edge)
        return edge.dup if edge.is_a?(Symbol) || edge.is_a?(String)

        edge.keys.first.dup
      end

      # Input: ?
      #
      # Output: Array[...String | Symbol]
      def extract_needs(edge)
        return [] unless edge.is_a?(Hash)

        name  = edge.keys.first
        needs = edge[name]

        if needs.is_a?(Array)
          needs.map(&:dup)
        else
          [needs.dup]
        end
      end
    end
  end
end
