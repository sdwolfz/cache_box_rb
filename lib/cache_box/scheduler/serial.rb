# frozen_string_literal: true

require_relative 'base'

class CacheBox
  module Scheduler
    class Serial < Base
      # Input:
      #
      # nodes = Hash{...String => Array[String | Symbol, Proc(Object)]}
      # unit  = CacheBox::Unit
      #
      # Output: Hash{...String | Symbol => Object}
      def run!(nodes, unit)
        plan = @leaves.dup.reverse

        plan_index = 0
        loop do
          name_s = plan[plan_index]

          unless unit.has?(name_s)
            needs = @needs[name_s]
            needs.reverse_each { |need| plan << need }
          end

          plan_index += 1
          break if plan_index >= plan.size
        end

        sequence = plan.reverse
        sequence.each_with_index do |name_s, index|
          next if unit.has?(name_s)

          callable = nodes[name_s][1]
          needs    = @needs[name_s]
          input    = {}

          needs.each do |need|
            next unless unit.has?(need)

            input[@lookup[need]] = unit.result(need)
          end
          callable.call(input)

          needs.each do |need|
            needed = false

            sequence[(index + 1)..].each do |item|
              needed = true if @needs[item].include?(need)
            end

            unit.clear(need) unless needed
          end
        end

        result = {}
        @leaves.each do |name_s|
          next unless unit.has?(name_s)

          result[@lookup[name_s]] = unit.result(name_s)
        end
        result
      end
    end
  end
end
