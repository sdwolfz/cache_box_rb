# frozen_string_literal: true

require_relative 'base'

class CacheBox
  module Scheduler
    class Concurrent < Base

      # TODO: write and use a thread pool

      # Input:
      #
      # nodes = Hash{...String => Array[String | Symbol, Proc(Object)]}
      # unit  = CacheBox::Unit
      #
      # Output: Hash{...String | Symbol => Object}
      def run!(nodes, unit)
        work    = @roots.dup
        threads = []

        condition = ConditionVariable.new

        condition_lock = Mutex.new
        scheduler_lock = Mutex.new

        scheduler = Thread.new do
          condition_lock.synchronize do
            index = 0
            loop do
              stop = false

              scheduler_lock.synchronize do
                loop do
                  break if index >= work.size

                  name_s = work[index]
                  thread = Thread.new do
                    callable = nodes[name_s][1]
                    needs    = @needs[name_s]
                    input    = {}

                    scheduler_lock.synchronize do
                      needs.each do |need|
                        next unless unit.has?(need)

                        input[@lookup[need]] = unit.result(need)
                      end
                    end

                    callable.call(input)

                    scheduler_lock.synchronize do
                      @feeds[name_s]&.each do |feed|
                        work.push(feed) unless work.include?(feed)
                      end

                      threads.delete(Thread.current)
                    end

                    condition_lock.synchronize do
                      condition.signal
                    end
                  end
                  threads << thread

                  index += 1
                end

                stop = true if threads.empty?
              end

              break if stop

              condition.wait(condition_lock)
            end
          end
        end

        scheduler.join

        result = {}
        @leaves.each do |name_s|
          next unless unit.has?(name_s)

          result[@lookup[name_s]] = unit.result(name_s)
        end
        result
      end
    end
  end
end
