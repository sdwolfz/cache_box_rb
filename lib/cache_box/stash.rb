# frozen_string_literal: true

class CacheBox
  # A thread safe, in memory, key value storage.
  #
  # The object itself is thread safe but it's contents is not.
  class Stash
    def initialize(state = nil)
      @lock  = Mutex.new
      @state = state || {}
    end

    def [](key)
      @lock.synchronize do
        @state[key]
      end
    end

    def []=(key, value)
      @lock.synchronize do
        @state[key] = value
      end
    end

    def delete(key)
      @lock.synchronize do
        @state.delete(key)
      end
    end

    def key?(key)
      @lock.synchronize do
        @state.key?(key)
      end
    end

    def with
      @lock.synchronize do
        yield(@state)
      end
    end
  end
end
