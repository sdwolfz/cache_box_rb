# frozen_string_literal: true

class CacheBox
  module Helper
    # Common validation methods.
    module Validate
      # Input:
      #
      # arg  = String | Symbol
      # name = String
      #
      # Output: arg
      def validate_string_or_symbol!(arg, name)
        return arg if arg.is_a?(Symbol) || arg.is_a?(String)

        klass = arg.class
        value = arg.inspect

        raise(ArgumentError, "#{name} must be a Symbol or String, got #{klass}: #{value}")
      end

      # Input:
      #
      # args = Array[String | Symbol]
      # name = String
      #
      # Output: args
      def validate_array_of_string_or_symbol!(args, name)
        unless args.is_a?(Array)
          klass = args.class
          value = args.inspect

          raise(
            ArgumentError,
            "#{name} must be an Array, got #{klass}: #{value}"
          )
        end

        args.each do |arg|
          next if arg.is_a?(Symbol) || arg.is_a?(String)

          klass = arg.class
          value = arg.inspect

          raise(
            ArgumentError,
            "#{name} must contain Symbol or String, got #{klass}: #{value} in #{args}"
          )
        end

        args
      end

      # Input:
      #
      # block = &block
      # name  = String
      #
      # Output: N/A
      def validate_block_presence!(block, name)
        return block if block

        raise(ArgumentError, "The `#{name}` method requires a block")
      end

      # Input:
      #
      # edge = String |
      #        Symbol |
      #        Hash{1 String | Symbol => String | Symbol | Array[...String | Symbol]}
      #
      # Output: edge
      def validate_edge!(edge)
        return edge if edge.is_a?(Symbol) || edge.is_a?(String)

        if edge.is_a?(Hash)
          validate_hash_edge!(edge)

          edge
        else
          klass = edge.class
          value = edge.inspect

          raise(
            ArgumentError,
            "edge has an unknown structure; #{klass}: #{value}"
          )
        end
      end

      private

      # Input:
      #
      # edge = Hash{1 String | Symbol => String | Symbol | Array[...String | Symbol]}
      #
      # Output: edge
      def validate_hash_edge!(edge)
        if (size = edge.size) != 1
          raise(
            ArgumentError,
            "edge must have one mapping, got #{size}: #{edge.inspect}"
          )
        end

        name  = edge.keys.first
        needs = edge[name]

        validate_string_or_symbol!(name, 'edge name')
        validate_needs!(needs)

        edge
      end

      # Input:
      #
      # arg = String | Symbol | Array[...String | Symbol]
      #
      # Output: arg
      def validate_needs!(arg)
        return arg if arg.is_a?(Symbol) || arg.is_a?(String)

        if arg.is_a?(Array)
          validate_array_of_string_or_symbol!(arg, 'edge needs')

          arg
        else
          klass = arg.class
          value = arg.inspect

          raise(
            ArgumentError,
            "edge needs has an unknown structure; #{klass}: #{value}"
          )
        end
      end
    end
  end
end
