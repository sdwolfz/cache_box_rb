# frozen_string_literal: true

class CacheBox
  # A container for references needed during execution.
  class Box
    attr_reader :namespace, :name, :args, :input, :stash, :logger

    def initialize(namespace:, name:, args:, input:, stash:, logger:)
      @namespace = namespace
      @name      = name
      @args      = args
      @input     = input
      @stash     = stash
      @logger    = logger
    end
  end
end
