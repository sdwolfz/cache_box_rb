# frozen_string_literal: true

class CacheBox
  module Storage
    # A storage backed by files.
    class File
      LOCK = Mutex.new

      # Input:
      #
      # namespace = Symbol | String # Default: 'namespace'
      # path      = String # Path; Default: nil
      #
      # Output: N/A
      def initialize(namespace: nil, path: nil)
        validate_symbol_or_string!(namespace, 'namespace')
        validate_string!(path, 'path')

        namespace = namespace.to_s || 'namespace'

        root  = path || ::File.join(Dir.pwd, '.cache')
        @path = ::File.join(root, namespace)
      end

      # Output: self
      def reset!
        LOCK.synchronize do
          FileUtils.remove_entry_secure(@path, true)
        end

        self
      end

      # Reads the content.
      #
      # Input:
      #
      # name = String
      #
      # Output: Object # Anything
      def read!(name)
        validate_string!(name, 'name')

        LOCK.synchronize do
          file = ::File.join(@path, name)
          return unless ::File.exist?(file)

          Marshal.load(::File.read(file))
        end
      end

      # Input:
      #
      # name = String
      # data = Object # Anything
      #
      # Output: self
      def write!(name, data)
        validate_string!(name, 'name')

        content = Marshal.dump(data)
        file    = ::File.join(@path, name)

        LOCK.synchronize do
          FileUtils.mkdir_p(@path)

          ::File.write(file, content)
        end

        self
      end

      # Input:
      #
      # name = String
      #
      # Output: self
      def delete!(name)
        validate_string!(name, 'name')

        LOCK.synchronize do
          file = ::File.join(@path, name)
          FileUtils.remove_entry_secure(file, true)
        end

        self
      end

      # Input:
      #
      # name = String
      #
      # Output: true | false
      def has?(name)
        validate_string!(name, 'name')

        LOCK.synchronize do
          file = ::File.join(@path, name)
          ::File.file?(file)
        end
      end

      private

      def validate_symbol_or_string!(arg, name)
        return if arg.nil? || arg.is_a?(Symbol) || arg.is_a?(String)

        klass = arg.class
        value = arg.inspect

        raise(
          ArgumentError,
          "#{name} must be a Symbol or String, got #{klass}: #{value}"
        )
      end

      def validate_string!(arg, name)
        return if arg.nil? || arg.is_a?(String)

        klass = arg.class
        value = arg.inspect

        raise(
          ArgumentError,
          "#{name} must be a String, got #{klass}: #{value}"
        )
      end
    end
  end
end
