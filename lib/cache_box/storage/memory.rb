# frozen_string_literal: true

class CacheBox
  module Storage
    # A storage backed by an in memory Hash.
    class Memory
      LOCK = Mutex.new

      # Accepts a Hash or any Hash-like object as argument. Will use a plain Hash
      # if none provided.
      #
      # Input:
      #
      # state = Hash{...Object => Object} # Default: nil
      #
      # Output: N/A
      def initialize(state = nil)
        validate!(state)

        LOCK.synchronize do
          @state = state || {}
        end
      end

      # Accepts a Hash or any Hash-like object as argument. Will use a plain Hash
      # if none provided.
      #
      # Input:
      #
      # state = Hash{...Object => Object}
      #
      # Output: self
      def reset!(state = nil)
        initialize(state)

        self
      end

      # Input:
      #
      # name = String
      #
      # Output: Object # Anything
      def read!(name)
        validate_string!(name, 'name')

        LOCK.synchronize do
          @state[name]
        end
      end

      # Input:
      #
      # name = String
      # data = Object # Anything
      #
      # Output: self
      def write!(name, data)
        validate_string!(name, 'name')

        LOCK.synchronize do
          @state[name] = data
        end

        self
      end

      # Input:
      #
      # name = String
      #
      # Output: self
      def delete!(name)
        validate_string!(name, 'name')

        LOCK.synchronize do
          @state.delete(name)
        end

        self
      end

      # Input:
      #
      # name = String
      #
      # Output: true | false
      def has?(name)
        validate_string!(name, 'name')

        LOCK.synchronize do
          @state.key?(name)
        end
      end

      private

      # Input:
      #
      # state = Object # Anything
      #
      # Output: N/A
      def validate!(state)
        return if state.nil? || state.is_a?(Hash)

        validate_get!(state)
        validate_set!(state)
        validate_delete!(state)
        validate_key!(state)
      end

      # Input:
      #
      # state = Object # Anything
      #
      # Output: N/A
      def validate_get!(state)
        unless state.respond_to?(:[])
          raise(ArgumentError, 'Given state object does not respond to `:[]`')
        end

        arity = state.method(:[]).arity
        unless arity == 1
          raise(
            ArgumentError,
            "Given state object's `:[]` method arity must be 1, got: #{arity}"
          )
        end
      end

      # Input:
      #
      # state = Object # Anything
      #
      # Output: N/A
      def validate_set!(state)
        unless state.respond_to?(:[]=)
          raise(ArgumentError, 'Given state object does not respond to `:[]=`')
        end

        arity = state.method(:[]=).arity
        unless arity == 2
          raise(
            ArgumentError,
            "Given state object's `:[]=` method arity must be 2, got: #{arity}"
          )
        end
      end

      # Input:
      #
      # state = Object # Anything
      #
      # Output: N/A
      def validate_delete!(state)
        unless state.respond_to?(:delete)
          raise(ArgumentError, 'Given state object does not respond to `delete`')
        end

        arity = state.method(:delete).arity
        unless arity == 1
          raise(
            ArgumentError,
            "Given state object's `:delete` method arity must be 1, got: #{arity}"
          )
        end
      end

      # Input:
      #
      # state = Object # Anything
      #
      # Output: N/A
      def validate_key!(state)
        unless state.respond_to?(:key?)
          raise(ArgumentError, 'Given state object does not respond to `:key?`')
        end

        arity = state.method(:key?).arity
        unless arity == 1
          raise(
            ArgumentError,
            "Given state object's `:key?` method arity must be 1, got: #{arity}"
          )
        end
      end

      # Input:
      #
      # arg  = String
      # name = String
      #
      # Output: N/A
      def validate_string!(arg, name)
        return if arg.nil? || arg.is_a?(String)

        klass = arg.class
        value = arg.inspect

        raise(
          ArgumentError,
          "#{name} must be a String, got #{klass}: #{value}"
        )
      end
    end
  end
end
