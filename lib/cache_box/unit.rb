# frozen_string_literal: true

class CacheBox
  class Unit
    include CacheBox::Helper::Validate

    LOCK = Mutex.new

    # Input:
    #
    # namespace = String | Symbol # Default: :namespace
    #
    # Output: N/A
    def initialize(namespace = nil, logger: nil, storage: nil)
      namespace ||= :namespace
      validate_string_or_symbol!(namespace, 'namespace')

      @namespace_o = namespace
      @namespace_s = namespace.to_s
      @logger      = logger  || Logger.new(STDOUT, level: Logger::INFO)
      @storage     = storage || ::CacheBox::Storage::File.new(namespace: @namespace_o)

      LOCK.synchronize do
        @state = { result: ::CacheBox::Stash.new, stash: ::CacheBox::Stash.new }
      end
    end

    # Input:
    #
    # name   = String | Symbol # Default: 'name'
    # args   = Object
    # input  = Object
    # &block = Proc(CacheBox::Box)
    #
    # Output: Object # Anything the &block returns.
    def with(name = nil, args = nil, input = nil, &block)
      name ||= 'name'
      validate_string_or_symbol!(name, 'name')
      validate_block_presence!(block, '#with')

      name_o = name
      name_s = name.to_s
      box    = nil

      LOCK.synchronize do
        return @state[:result][name_s] if @state[:result].key?(name_s)

        data = @storage.read!(name_s)
        if data&.key?(:result)
          @state[:result][name_s] = data[:result]

          return data[:result]
        elsif data&.key?(:stash)
          @state[:stash][name_s] = data[:stash]
        end

        box = ::CacheBox::Box.new(
          namespace: @namespace_o.dup,
          name:      name_o.dup,
          args:      args,
          input:     input,
          stash:     @state[:stash][name] || ::CacheBox::Stash.new,
          logger:    @logger
        )
      end

      begin
        result = block.call(box)

        LOCK.synchronize do
          @state[:result][name_s] = result
        end
      ensure
        LOCK.synchronize do
          if @state[:result].key?(name_s)
            @state[:stash].delete(name_s)
            @storage.write!(name_s, { result: @state[:result][name_s] })
          else
            stash = box.stash
            stash = ::CacheBox::Stash.new(stash) if stash.is_a?(Hash)

            stash.with do |state|
              dump = { stash: state }

              @state[:stash][name_s] = stash
              @storage.write!(name_s, dump)
            end
          end
        end
      end
    end

    # Input:
    #
    # name = String | Symbol # Default: 'name'
    #
    # Output: true | false
    def has?(name = nil)
      name ||= 'name'
      validate_string_or_symbol!(name, 'name')
      name = name.to_s

      LOCK.synchronize do
        return true if @state[:result].key?(name)

        data = @storage.read!(name)

        if data&.key?(:result)
          @state[:result][name] = data[:result]
        elsif data&.key?(:stash)
          @state[:stash][name] = data[:stash]
        end

        @state[:result].key?(name)
      end
    end

    # Input:
    #
    # name = String | Symbol # Default: 'name'
    #
    # Output: true | false
    def result(name = nil)
      name ||= 'name'
      validate_string_or_symbol!(name, 'name')
      name = name.to_s

      LOCK.synchronize do
        return @state[:result][name] if @state[:result].key?(name)

        data = @storage.read!(name)

        if data&.key?(:result)
          @state[:result][name] = data[:result]
        elsif data&.key?(:stash)
          @state[:stash][name] = data[:stash]
        end

        @state[:result][name]
      end
    end

    # Input:
    #
    # name = Array[String | Symbol] # Default: []
    #
    # Output: self
    def expire!(*names)
      validate_array_of_string_or_symbol!(names, 'names')

      if names.empty?
        LOCK.synchronize do
          @state = { result: ::CacheBox::Stash.new, stash: ::CacheBox::Stash.new }

          @storage.reset!
        end
      else
        LOCK.synchronize do
          names.each do |name_o|
            name_s = name_o.to_s

            @state[:result].delete(name_s)
            @state[:stash].delete(name_s)

            @storage.delete!(name_s)
          end
        end
      end

      self
    end

    # Input:
    #
    # name = Array[String | Symbol] # Default: []
    #
    # Output: self
    def clear(*names)
      validate_array_of_string_or_symbol!(names, 'names')

      if names.empty?
        LOCK.synchronize do
          @state = { result: ::CacheBox::Stash.new, stash: ::CacheBox::Stash.new }
        end
      else
        LOCK.synchronize do
          names.each do |name_o|
            name_s = name_o.to_s

            @state[:result].delete(name_s)
            @state[:stash].delete(name_s)
          end
        end
      end

      self
    end
  end
end
