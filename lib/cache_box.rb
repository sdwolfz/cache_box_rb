# frozen_string_literal: true

require 'fileutils'
require 'logger'

require_relative 'cache_box/helper/validate'

require_relative 'cache_box/storage/file'
require_relative 'cache_box/storage/memory'

require_relative 'cache_box/stash'
require_relative 'cache_box/box'
require_relative 'cache_box/unit'

require_relative 'cache_box/scheduler/concurrent'
require_relative 'cache_box/scheduler/serial'

require_relative 'cache_box/chain'
require_relative 'cache_box/graph'

class CacheBox
  include CacheBox::Helper::Validate

  # Input:
  #
  # namespace = String | Symbol # Default: :namespace
  # logger    = Logger
  # storage   = CacheBox::Storage::*
  #
  # Output: N/A
  def initialize(namespace = nil, logger: nil, storage: nil)
    namespace ||= :namespace
    validate_string_or_symbol!(namespace, 'namespace')

    @unit = ::CacheBox::Unit.new(namespace, logger: logger, storage: storage)
  end

  # Input:
  #
  # name   = String | Symbol # Default: 'name'
  # args   = Object
  # &block = Proc(CacheBox::Box)
  #
  # Output: Object # Anything the &block returns.
  def with(name = nil, args = nil, &block)
    name ||= 'name'
    validate_string_or_symbol!(name, 'name')
    validate_block_presence!(block, '#with')

    @unit.with(name, args, nil, &block)
  end

  # Input:
  #
  # name = String | Symbol # Default: 'name'
  #
  # Output: true | false
  def has?(name = nil)
    name ||= 'name'
    validate_string_or_symbol!(name, 'name')

    @unit.has?(name)
  end

  # Input:
  #
  # name = Array[String | Symbol] # Default: []
  #
  # Output: self
  def expire!(*names)
    validate_array_of_string_or_symbol!(names, 'names')

    @unit.expire!(*names)

    self
  end
end
