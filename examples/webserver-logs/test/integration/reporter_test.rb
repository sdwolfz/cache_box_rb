# frozen_string_literal: true

require 'test_helper'
require 'tempfile'
require 'lib/reporter'

describe Reporter do
  let(:described_class) { Reporter }

  describe '#initialize' do
    it 'can be instantiated without arguments' do
      subject = described_class.new

      assert_instance_of(described_class, subject)
    end

    it 'receives a logger as argument' do
      logger  = nil
      subject = described_class.new(logger: logger)
      assert_instance_of(described_class, subject)

      logger  = Logger.new($stdout)
      subject = described_class.new(logger: logger)
      assert_instance_of(described_class, subject)
    end
  end

  describe '#call' do
    let(:io)      { StringIO.new }
    let(:logger)  { Logger.new(io) }
    let(:subject) { described_class.new(logger: logger) }

    it 'reads from the input log and generates reports' do
      Tempfile.create('', File.join('test', '.tmp')) do |input_file|
        Dir.mktmpdir(nil, File.join('test', '.tmp')) do |dir|
          input_data = <<~FILE
            /hello 127.0.0.1
            /hello 127.0.0.1
            /goodbye 127.0.0.1
            /goodbye 127.0.0.2
            /goodbye 127.0.0.3
          FILE

          output_dir = File.path(dir)
          input_path = File.path(input_file)
          File.write(input_path, input_data)

          subject.call(input_path, output_dir)

          hits_data = <<~FILE
            /goodbye 3 visits
            /hello 2 visits
          FILE

          unique_data = <<~FILE
            /goodbye 3 unique views
            /hello 1 unique views
          FILE

          hits_result   = File.read(File.join(output_dir, 'hits.log'))
          unique_result = File.read(File.join(output_dir, 'unique.log'))

          assert_equal(hits_data, hits_result)
          assert_equal(unique_data, unique_result)
        end
      end
    end
  end
end
