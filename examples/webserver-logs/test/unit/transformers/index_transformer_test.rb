# frozen_string_literal: true

require 'test_helper'
require 'lib/transformers/index_transformer'

describe Transformers::IndexTransformer do
  let(:described_class) { Transformers::IndexTransformer }

  describe '#initialize' do
    it 'can be instantiated without arguments' do
      subject = described_class.new

      assert_instance_of(described_class, subject)
    end

    it 'receives a logger as argument' do
      logger  = nil
      subject = described_class.new(logger: logger)
      assert_instance_of(described_class, subject)

      logger  = Logger.new($stdout)
      subject = described_class.new(logger: logger)
      assert_instance_of(described_class, subject)
    end
  end

  describe '#call' do
    let(:io)      { StringIO.new }
    let(:logger)  { Logger.new(io) }
    let(:subject) { described_class.new(logger: logger) }

    it 'returns an empty Hash when data is empty' do
      data     = ''
      expected = {}
      result   = subject.call(data)

      assert_equal(expected, result)
    end

    it 'adds data to the index with a counter' do
      data = <<~FILE
        /hello 127.0.0.1
      FILE
      expected = { '/hello' => { '127.0.0.1' => 1 } }
      result   = subject.call(data)
      assert_equal(expected, result)

      io.rewind
      log = io.read
      assert_match(%r{^.*?Initializing /hello => 127.0.0.1$}, log)
    end

    it 'increments the counter when the same data is pushed' do
      data = <<~FILE
        /hello 127.0.0.1
        /hello 127.0.0.1
      FILE
      expected = { '/hello' => { '127.0.0.1' => 2 } }
      result   = subject.call(data)
      assert_equal(expected, result)

      io.rewind
      log = io.read.lines
      assert_match(%r{^.*?Initializing /hello => 127.0.0.1$}, log[0])
      assert_match(%r{^.*?Incrementing /hello => 127.0.0.1$}, log[1])
    end

    it 'sets a new counter for different IPs' do
      data = <<~FILE
        /hello 127.0.0.1
        /hello 127.0.0.2
      FILE
      expected = { '/hello' => { '127.0.0.1' => 1, '127.0.0.2' => 1 } }
      result   = subject.call(data)
      assert_equal(expected, result)

      io.rewind
      log = io.read.lines
      assert_match(%r{^.*?Initializing /hello => 127.0.0.1$}, log[0])
      assert_match(%r{^.*?Setting /hello => 127.0.0.2$}, log[1])
    end

    it 'indexes multiple paths' do
      data = <<~FILE
        /hello 127.0.0.1
        /goodbye 127.0.0.2
      FILE
      expected = {
        '/hello'   => { '127.0.0.1' => 1 },
        '/goodbye' => { '127.0.0.2' => 1 }
      }
      result = subject.call(data)
      assert_equal(expected, result)

      io.rewind
      log = io.read.lines
      assert_match(%r{^.*?Initializing /hello => 127.0.0.1$}, log[0])
      assert_match(%r{^.*?Initializing /goodbye => 127.0.0.2$}, log[1])
    end
  end
end
