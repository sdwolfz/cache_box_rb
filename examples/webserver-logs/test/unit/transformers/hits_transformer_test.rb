# frozen_string_literal: true

require 'test_helper'
require 'lib/transformers/hits_transformer'

describe Transformers::HitsTransformer do
  let(:described_class) { Transformers::HitsTransformer }

  describe '#initialize' do
    it 'can be instantiated without arguments' do
      subject = described_class.new

      assert_instance_of(described_class, subject)
    end

    it 'receives a logger as argument' do
      logger  = nil
      subject = described_class.new(logger: logger)
      assert_instance_of(described_class, subject)

      logger  = Logger.new($stdout)
      subject = described_class.new(logger: logger)
      assert_instance_of(described_class, subject)
    end
  end

  describe '#call' do
    let(:io)      { StringIO.new }
    let(:logger)  { Logger.new(io) }
    let(:subject) { described_class.new(logger: logger) }

    it 'transform the index into a hit list' do
      index    = { '/hello' => { '127.0.0.1' => 2, '127.0.0.2' => 3 } }
      expected = { '/hello' => 5 }
      result   = subject.call(index)
      assert_equal(expected, result)

      io.rewind
      log = io.read
      assert_match(%r{^.*?Path: /hello Total: 5$}, log)
    end
  end
end
