# frozen_string_literal: true

require 'test_helper'
require 'lib/transformers/unique_transformer'

describe Transformers::UniqueTransformer do
  let(:described_class) { Transformers::UniqueTransformer }

  describe '#initialize' do
    it 'can be instantiated without arguments' do
      subject = described_class.new

      assert_instance_of(described_class, subject)
    end

    it 'receives a logger as argument' do
      logger  = nil
      subject = described_class.new(logger: logger)
      assert_instance_of(described_class, subject)

      logger  = Logger.new($stdout)
      subject = described_class.new(logger: logger)
      assert_instance_of(described_class, subject)
    end
  end

  describe '#call' do
    let(:io)      { StringIO.new }
    let(:logger)  { Logger.new(io) }
    let(:subject) { described_class.new(logger: logger) }

    it 'transform the index into a uniqueness list' do
      index    = { '/hello' => { '127.0.0.1' => 1, '127.0.0.2' => 3 } }
      expected = { '/hello' => 2 }
      result   = subject.call(index)
      assert_equal(expected, result)

      io.rewind
      log = io.read
      assert_match(%r{^.*?Path: /hello Unique: 2$}, log)

    end
  end
end
