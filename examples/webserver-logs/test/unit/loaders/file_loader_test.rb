# frozen_string_literal: true

require 'test_helper'
require 'tempfile'
require 'lib/loaders/file_loader'

describe Loaders::FileLoader do
  let(:described_class) { Loaders::FileLoader }

  describe '#initialize' do
    it 'can be instantiated without arguments' do
      subject = described_class.new

      assert_instance_of(described_class, subject)
    end

    it 'receives a logger as argument' do
      logger  = nil
      subject = described_class.new(logger: logger)
      assert_instance_of(described_class, subject)

      logger  = Logger.new($stdout)
      subject = described_class.new(logger: logger)
      assert_instance_of(described_class, subject)
    end
  end

  describe '#call' do
    let(:io)      { StringIO.new }
    let(:logger)  { Logger.new(io) }
    let(:subject) { described_class.new(logger: logger) }

    it 'loads the data into a file' do
      Tempfile.create('', File.join('test', '.tmp')) do |file|
        path = File.path(file)
        data = ['/hello 6 visits', '/goodbye 5 visits']
        result = subject.call(data, path)
        assert_same(result, subject)

        io.rewind
        log = io.read.lines
        assert_match(%r{^.*?Loading into test/.tmp/.*$},  log[0])
        assert_match(%r{^.*?Loading: /hello 6 visits$},   log[1])
        assert_match(%r{^.*?Loading: /goodbye 5 visits$}, log[2])

        content = File.read(File.path(file))
        expected = <<~FILE
          /hello 6 visits
          /goodbye 5 visits
        FILE

        assert_equal(expected, content)
      end
    end
  end
end
