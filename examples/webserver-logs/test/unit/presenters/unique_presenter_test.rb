# frozen_string_literal: true

require 'test_helper'
require 'lib/presenters/unique_presenter'

describe Presenters::UniquePresenter do
  let(:described_class) { Presenters::UniquePresenter }

  describe '#initialize' do
    it 'can be instantiated without arguments' do
      subject = described_class.new

      assert_instance_of(described_class, subject)
    end

    it 'receives a logger as argument' do
      logger  = nil
      subject = described_class.new(logger: logger)
      assert_instance_of(described_class, subject)

      logger  = Logger.new($stdout)
      subject = described_class.new(logger: logger)
      assert_instance_of(described_class, subject)
    end
  end

  describe '#call' do
    let(:io)      { StringIO.new }
    let(:logger)  { Logger.new(io) }
    let(:subject) { described_class.new(logger: logger) }

    it 'transforms the hit list into an ordered presentable array' do
      hits     = { '/hello' => 5, '/donut' => 4, '/goodbye' => 6 }
      result   = subject.call(hits)
      expected = [
        '/goodbye 6 unique views',
        '/hello 5 unique views',
        '/donut 4 unique views'
      ]
      assert_equal(expected, result)

      io.rewind
      log = io.read.lines
      assert_match(%r{^.*?Presenting: /goodbye 6$}, log[0])
      assert_match(%r{^.*?Presenting: /hello 5$}, log[1])
      assert_match(%r{^.*?Presenting: /donut 4$}, log[2])
    end
  end
end
