# frozen_string_literal: true

require 'test_helper'
require 'tempfile'
require 'lib/extractors/file_extractor'

describe Extractors::FileExtractor do
  let(:described_class) { Extractors::FileExtractor }

  describe '#initialize' do
    it 'can be instantiated without arguments' do
      subject = described_class.new

      assert_instance_of(described_class, subject)
    end

    it 'receives a logger as argument' do
      logger  = nil
      subject = described_class.new(logger: logger)
      assert_instance_of(described_class, subject)

      logger  = Logger.new($stdout)
      subject = described_class.new(logger: logger)
      assert_instance_of(described_class, subject)
    end
  end

  describe '#call' do
    let(:io)      { StringIO.new }
    let(:logger)  { Logger.new(io) }
    let(:subject) { described_class.new(logger: logger) }

    it 'reads data from a file' do
      Tempfile.create('', File.join('test', '.tmp')) do |file|
        path = File.path(file)
        data = <<~FILE
          /hello 127.0.0.1
          /goodbye 127.0.0.2
        FILE
        File.write(path, data)

        result = subject.call(path)
        assert_equal(data, result)

        io.rewind
        log = io.read
        assert_match(%r{^.*?Extracting test/.tmp/.*$}, log)
      end
    end
  end
end
