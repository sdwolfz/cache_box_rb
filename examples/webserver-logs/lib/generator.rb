require 'logger'

# Generates 100% real life webserver log data, you can trust me on this one!
class Generator
  PARTS = [
    'about',
    'cookies',
    'donuts',
    'home',
    'meat',
    'posts',
    'users'
  ].freeze

  IPS = [
    '0.0.0.0',
    '1.1.1.1',
    '127.0.0.1',
    '192.168.0.1',
    '35.185.44.232'
  ].freeze

  def initialize(logger: nil)
    @logger = logger || Logger.new($stdout)
  end

  def call(path, size)
    File.open(path, 'w+') do |file|
      with_generated_paths(size) do |generated|
        file.puts generated
      end
    end
  end

  # ----------------------------------------------------------------------------
  # Private

  private

  def with_generated_paths(size)
    size.times do
      parts = rand(3) + 1

      result = if parts == 1
                 PARTS[parts]
               else
                 first  = PARTS[rand(PARTS.size)]
                 second = PARTS[rand(PARTS.size)]

                 "#{first}/#{second}"
               end
      result = "#{result}/#{rand(10) + 1}" if parts == 3
      ip     = IPS[rand(IPS.size)]

      yield "/#{result} #{ip}"
    end
  end
end
