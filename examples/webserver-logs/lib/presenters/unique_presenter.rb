# frozen_string_literal: true

require 'logger'

module Presenters
  # Makes the unique list presentable.
  class UniquePresenter
    def initialize(logger: nil)
      @logger = logger || Logger.new($stdout)
    end

    def call(hits)
      hits
        .to_a
        .sort_by { |_path, count| count }
        .reverse
        .map do |list|
        @logger.debug("Presenting: #{list[0]} #{list[1]}")

        "#{list[0]} #{list[1]} unique views"
      end
    end
  end
end
