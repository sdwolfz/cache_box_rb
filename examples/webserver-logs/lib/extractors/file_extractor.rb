# frozen_string_literal: true

module Extractors
  # Extracts raw data from files.
  class FileExtractor
    def initialize(logger: nil)
      @logger = logger || Logger.new($stdout)
    end

    def call(path)
      @logger.info("Extracting #{path}")

      File.read(path)
    end
  end
end
