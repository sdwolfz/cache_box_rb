# frozen_string_literal: true

# Gotta look sharp!
module Presenters
end

require_relative './presenters/hits_presenter'
require_relative './presenters/unique_presenter'
