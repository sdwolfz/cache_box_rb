# frozen_string_literal: true

module Loaders
  class FileLoader
    def initialize(logger: nil)
      @logger = logger || Logger.new($stdout)
    end

    def call(data, path)
      @logger.info("Loading into #{path}")

      FileUtils.mkdir_p(File.dirname(path))
      File.open(File.join(path), 'w+') do |file|
        data.each do |element|
          @logger.debug("Loading: #{element}")

          file.puts(element)
        end
      end

      self
    end
  end
end
