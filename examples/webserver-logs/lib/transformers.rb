# frozen_string_literal: true

# Robots in disguise!
module Transformers
end

require_relative './transformers/hits_transformer'
require_relative './transformers/index_transformer'
require_relative './transformers/unique_transformer'
