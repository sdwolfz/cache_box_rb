require 'logger'

require_relative './extractors'
require_relative './loaders'
require_relative './presenters'
require_relative './transformers'

require 'cache_box'

class Reporter
  def initialize(logger: nil)
    @logger  = logger || Logger.new($stdout)
  end

  def call(input_path, output_dir)
    # --------------------------------------------------------------------------
    # Setup

    scheduler = CacheBox::Scheduler::Concurrent.new
    storage   = CacheBox::Storage::Memory.new
    graph     = CacheBox::Graph.new(:graph, scheduler: scheduler, storage: storage)

    # --------------------------------------------------------------------------
    # Indexing

    graph.node(:extractor) do |_box|
      Extractors::FileExtractor.new(logger:  @logger).call(input_path)
    end

    graph.node(indexer: :extractor) do |box|
      content = box.input[:extractor]

      Transformers::IndexTransformer.new(logger: @logger).call(content)
    end

    # --------------------------------------------------------------------------
    # Hit count

    graph.node(hit_transformer: :indexer) do |box|
      index = box.input[:indexer]

      Transformers::HitsTransformer.new(logger: @logger).call(index)
    end

    graph.node(hit_presenter: :hit_transformer) do |box|
      hits = box.input[:hit_transformer]

      Presenters::HitsPresenter.new(logger: @logger).call(hits)
    end

    graph.node(hit_loader: :hit_presenter) do |box|
      output = box.input[:hit_presenter]
      file   = 'hits.log'
      path   = File.join(output_dir, file)

      Loaders::FileLoader.new(logger: @logger).call(output, path)
    end

    # --------------------------------------------------------------------------
    # Unique count

    graph.node(unique_transformer: :indexer) do |box|
      index = box.input[:indexer]

      Transformers::UniqueTransformer.new(logger: @logger).call(index)
    end

    graph.node(unique_presenter: :unique_transformer) do |box|
      uniques = box.input[:unique_transformer]

      Presenters::UniquePresenter.new(logger: @logger).call(uniques)
    end

    graph.node(unique_loader: :unique_presenter) do |box|
      output = box.input[:unique_presenter]
      file   = 'unique.log'
      path   = File.join(output_dir, file)

      Loaders::FileLoader.new(logger: @logger).call(output, path)
    end

    # --------------------------------------------------------------------------
    # Execute

    graph.run!
  end
end
