require 'logger'

module Transformers
  class IndexTransformer
    def initialize(logger: nil)
      @logger = logger || Logger.new($stdout)
    end

    def call(data)
      state = {}

      data.lines.each do |line|
        parts = line.split(' ')
        path  = parts[0].strip
        ip    = parts[1].strip

        if state.key?(path)
          if state[path].key?(ip)
            @logger.debug("Incrementing #{path} => #{ip}")

            state[path][ip] += 1
          else
            @logger.debug("Setting #{path} => #{ip}")

            state[path][ip] = 1
          end
        else
          @logger.debug("Initializing #{path} => #{ip}")

          state[path] = { ip => 1 }
        end
      end

      state
    end
  end
end
