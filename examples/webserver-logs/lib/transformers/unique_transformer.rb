# frozen_string_literal: true

require 'logger'

module Transformers
  # Calculates the number of unique hits for all urls in the index.
  class UniqueTransformer
    def initialize(logger: nil)
      @logger = logger || Logger.new($stdout)
    end

    def call(index)
      hits = {}

      index.to_a.each do |path, stats|
        count = stats.map { |_ip, total| total }.size

        @logger.debug("Path: #{path} Unique: #{count}")
        hits[path] = count
      end

      hits
    end
  end
end
