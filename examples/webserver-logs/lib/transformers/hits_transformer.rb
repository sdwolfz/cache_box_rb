# frozen_string_literal: true

require 'logger'

module Transformers
  # Calculates the total number of hits for all urls in the index.
  class HitsTransformer
    def initialize(logger: nil)
      @logger = logger || Logger.new($stdout)
    end

    def call(index)
      hits = {}

      index.to_a.each do |path, stats|
        count = stats.map { |_ip, total| total }.sum

        @logger.debug("Path: #{path} Total: #{count}")
        hits[path] = count
      end

      hits
    end
  end
end
