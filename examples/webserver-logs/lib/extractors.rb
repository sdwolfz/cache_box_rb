# frozen_string_literal: true

# Mining for gold and data.
module Extractors
end

require_relative './extractors/file_extractor'
