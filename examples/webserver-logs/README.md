# Webserver Logs Processing Example

## Prerequisites

| Name     | Link                               |
|----------|------------------------------------|
| `bash`   | https://www.gnu.org/software/bash  |
| `docker` | https://docs.docker.com/get-docker |
| `make`   | https://www.gnu.org/software/make  |

## Build

To ensure smooth sailing, build the docker image and start a shell with:

```sh
make build shell
```

You should now have a prompt in a ruby container where the rest of the commands
can be executed!

## Generate

Run `make generate` to create a `data/input.log` file with our 100% legit and
real life HTTP server log data.

## Run

Execute the report script with `make run`. This will generate two output files:
* `./data/output/hits.log`
* `./data/output/unique.log`

## CI

The project has a CI pipeline that can be invoked with `make ci` on the host
machine (not in the shell started above with `make shell`). To be considered a
success check the exit code of the command with: `echo $?`, it must be `0`.

You can also run individual checks with one of these:

```sh
make ci/eclint
make ci/hadolint
make ci/yamllint
make ci/rubocop
make ci/minitest
```

### Linters

* `editorconfig` - Checks line endings and whitespace errors.
* `yamllint` - Ensures YAML files are formattter properly.
* `hadolint` - Verifies the `Dockerfile`.
* `rubocop` - Ignores all of the overly dramatic ruby linter errors :).

### Tests

* `minitest` - Testing framework chosen for the project.
