# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name    = 'cache_box'
  spec.version = '0.0.1-preview10'
  spec.authors = ['Codruț Constantin Gușoi']
  spec.email   = ['mail+rubygems@codrut.pro']

  spec.summary  = 'An ETL pipeline library with implicit caching.'
  spec.homepage = 'https://gitlab.com/sdwolfz/cache_box_rb'
  spec.license  = 'BSD-3-Clause'

  spec.metadata = {
    'homepage_uri'    => 'https://gitlab.com/sdwolfz/cache_box_rb',
    'source_code_uri' => 'https://gitlab.com/sdwolfz/cache_box_rb',

    'rubygems_mfa_required' => 'true'
  }

  spec.required_ruby_version = Gem::Requirement.new('>= 3.0.0')

  spec.files = [
    'lib/cache_box.rb',
    'lib/cache_box/helper/validate.rb',
    'lib/cache_box/scheduler/base.rb',
    'lib/cache_box/scheduler/concurrent.rb',
    'lib/cache_box/scheduler/serial.rb',
    'lib/cache_box/storage/file.rb',
    'lib/cache_box/storage/memory.rb',
    'lib/cache_box/box.rb',
    'lib/cache_box/chain.rb',
    'lib/cache_box/graph.rb',
    'lib/cache_box/stash.rb',
    'lib/cache_box/unit.rb',
    'LICENSE',
    'cache_box.gemspec'
  ]
  spec.require_paths = ['lib']

  spec.add_development_dependency 'minitest',         '~> 5.16'
  spec.add_development_dependency 'pry-byebug',       '~> 3.9'
  spec.add_development_dependency 'rake',             '~> 13.0'
  spec.add_development_dependency 'rubocop',          '~> 1.32'
  spec.add_development_dependency 'rubocop-minitest', '~> 0.21'
  spec.add_development_dependency 'rubocop-rake',     '~> 0.6'
  spec.add_development_dependency 'simplecov',        '~> 0.21'
end
