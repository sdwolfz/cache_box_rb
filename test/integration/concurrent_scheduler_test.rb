# frozen_string_literal: true

require 'test_helper'

describe CacheBox::Scheduler::Concurrent do
  let(:described_class) { CacheBox::Scheduler::Concurrent }
  let(:root) { File.join(Dir.pwd, 'test', '.temp') }
  let(:namespace) { 'namespace' }

  describe '#run!' do
    it 'runs the graph in the right order' do
      checks = proc do |storage|
        subject = described_class.new

        subject.add(:a)
        subject.add(:c)
        subject.add(:b)
        subject.add(d: :a)
        subject.add(e: :b)
        subject.add(f: :d)
        subject.add(g: :e)

        effect = [[], [], []]

        unit  = CacheBox::Unit.new(namespace, storage: storage)
        nodes = {
          'a' => ['a', proc { unit.with(:a) { effect[0] << 1 } }],
          'b' => ['b', proc { unit.with(:b) { effect[0] << 2 } }],
          'c' => ['c', proc { unit.with(:c) { effect[0] << 3 } }],
          'd' => ['d', proc { unit.with(:d) { effect[1] << 4 } }],
          'e' => ['e', proc { unit.with(:e) { effect[1] << 5 } }],
          'f' => ['f', proc { unit.with(:f) { effect[2] << 6 } }],
          'g' => ['g', proc { unit.with(:g) { effect[2] << 7 } }]
        }

        subject.run!(nodes, unit)

        result   = effect.map(&:sort)
        expected = [[1, 2, 3], [4, 5], [6, 7]]
        assert_equal(expected, result)
      end

      Dir.mktmpdir(nil, root) do |dir|
        file_storage = CacheBox::Storage::File.new(namespace: namespace, path: dir)

        dir_path = File.join(dir, namespace)
        FileUtils.mkdir_p(dir_path)

        checks.call(file_storage)
      end

      memory_storage = CacheBox::Storage::Memory.new
      checks.call(memory_storage)
    end
  end
end
