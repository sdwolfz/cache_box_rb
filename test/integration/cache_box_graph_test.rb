# frozen_string_literal: true

require 'test_helper'

describe CacheBox::Graph do
  let(:described_class) { CacheBox::Graph }
  let(:root) { File.join(Dir.pwd, 'test', '.temp') }

  describe '#expire!' do
    it 'expires the cache' do
      checks = proc do |storage, scheduler|
        subject = described_class.new(storage: storage, scheduler: scheduler)

        assert_equal(false, storage.has?('key'))
        subject.node(:key) { 42 }
        subject.run!
        assert_equal(true, storage.has?('key'))

        subject.expire!(:key)
        assert_equal(false, storage.has?('key'))

        assert_equal(false, storage.has?('key_1'))
        assert_equal(false, storage.has?('key_2'))
        subject.node(:key_1) { 42 }
        subject.node(:key_2) { 24 }
        subject.run!
        assert_equal(true, storage.has?('key_1'))
        assert_equal(true, storage.has?('key_2'))

        subject.expire!
        assert_equal(false, storage.has?('key_1'))
        assert_equal(false, storage.has?('key_2'))
      end

      Dir.mktmpdir(nil, root) do |dir|
        namespace    = 'namespace'
        file_storage = CacheBox::Storage::File.new(namespace: namespace, path: dir)

        dir_path = File.join(dir, namespace)
        FileUtils.mkdir_p(dir_path)

        serial_scheduler     = CacheBox::Scheduler::Serial.new
        concurrent_scheduler = CacheBox::Scheduler::Concurrent.new

        checks.call(file_storage, serial_scheduler)
        checks.call(file_storage, concurrent_scheduler)
      end

      memory_storage = CacheBox::Storage::Memory.new

      serial_scheduler     = CacheBox::Scheduler::Serial.new
      concurrent_scheduler = CacheBox::Scheduler::Concurrent.new

      checks.call(memory_storage, serial_scheduler)
      checks.call(memory_storage, concurrent_scheduler)
    end
  end

  describe '#run!' do
    it 'recovers from failure without loading obsolete steps' do
      checks = proc do |storage, scheduler|
        subject = described_class.new(storage: storage, scheduler: scheduler)
        effect  = []
        crash   = true

        subject.node(:first, [1, 2]) do |box|
          assert_equal([1, 2], box.args)
          assert_equal({},     box.input)

          effect << :first_effect

          42
        end
        subject.node(:second, [3, 4]) do |box|
          assert_equal([3, 4], box.args)
          assert_equal({},     box.input)

          effect << :second_effect

          10
        end
        subject.node(:third) do |box|
          assert_nil(box.args)
          assert_equal({},  box.input)

          raise 'Crashing!' if crash

          effect << :third_effect

          100
        end

        error = assert_raises(StandardError) { subject.run! }
        assert_equal('Crashing!', error.message)
        assert_equal([:first_effect, :second_effect], effect)

        effect.clear
        crash  = false
        result = subject.run!
        assert_equal({ first: 42, second: 10, third: 100 }, result)
        assert_equal([:third_effect], effect)
      end

      Dir.mktmpdir(nil, root) do |dir|
        namespace    = 'namespace'
        file_storage = CacheBox::Storage::File.new(namespace: namespace, path: dir)

        dir_path = File.join(dir, namespace)
        FileUtils.mkdir_p(dir_path)

        serial_scheduler = CacheBox::Scheduler::Serial.new
        # concurrent_scheduler = CacheBox::Scheduler::Concurrent.new

        checks.call(file_storage, serial_scheduler)
        # checks.call(file_storage, concurrent_scheduler)
      end

      memory_storage = CacheBox::Storage::Memory.new

      serial_scheduler = CacheBox::Scheduler::Serial.new
      # concurrent_scheduler = CacheBox::Scheduler::Concurrent.new

      checks.call(memory_storage, serial_scheduler)
      # checks.call(memory_storage, concurrent_scheduler)
    end

    it 'holds progress in stash for later reuse' do
      checks = proc do |storage, scheduler|
        subject = described_class.new(storage: storage, scheduler: scheduler)
        effect  = []

        subject.node(:first) do |box|
          assert_nil(box.args)
          assert_equal({}, box.input)

          effect << :first_effect

          42
        end
        subject.node(:second, [1, 2]) do |box|
          assert_equal([1, 2], box.args)
          assert_equal({},     box.input)

          result = nil
          if box.stash.key?(:key)
            result = box.stash[:key]

            effect << :second_effect_result
          else
            box.stash[:key] = box.args
            effect << :second_effect_stash

            raise 'Crashing!'
          end

          result
        end
        subject.node(:third) do |box|
          assert_nil(box.args)
          assert_equal({}, box.input)

          effect << :third_effect

          100
        end

        error = assert_raises(StandardError) { subject.run! }
        assert_equal('Crashing!', error.message)
        assert_equal([:first_effect, :second_effect_stash], effect)

        effect.clear
        result = subject.run!
        assert_equal({ first: 42, second: [1, 2], third: 100 }, result)
        assert_equal([:second_effect_result, :third_effect], effect)
      end

      Dir.mktmpdir(nil, root) do |dir|
        namespace    = 'namespace'
        file_storage = CacheBox::Storage::File.new(namespace: namespace, path: dir)

        dir_path = File.join(dir, namespace)
        FileUtils.mkdir_p(dir_path)

        serial_scheduler = CacheBox::Scheduler::Serial.new
        # concurrent_scheduler = CacheBox::Scheduler::Concurrent.new

        checks.call(file_storage, serial_scheduler)
        # checks.call(file_storage, concurrent_scheduler)
      end

      memory_storage = CacheBox::Storage::Memory.new

      serial_scheduler = CacheBox::Scheduler::Serial.new
      # concurrent_scheduler = CacheBox::Scheduler::Concurrent.new

      checks.call(memory_storage, serial_scheduler)
      # checks.call(memory_storage, concurrent_scheduler)
    end

    it 'passes results from one job to another' do
      checks = proc do |storage, scheduler|
        subject = described_class.new(storage: storage, scheduler: scheduler)

        subject.node(:first) do |box|
          assert_nil(box.args)
          assert_equal({}, box.input)

          42
        end
        subject.node(:second, [1, 2]) do |box|
          assert_equal([1, 2], box.args)
          assert_equal({},     box.input)

          box.args
        end
        subject.node(third: [:first, :second]) do |box|
          assert_nil(box.args)
          assert_equal({ first: 42, second: [1, 2] }, box.input)

          box.input[:first] + box.input[:second].sum
        end

        result = subject.run!
        assert_equal({ third: 45 }, result)
      end

      Dir.mktmpdir(nil, root) do |dir|
        namespace    = 'namespace'
        file_storage = CacheBox::Storage::File.new(namespace: namespace, path: dir)

        dir_path = File.join(dir, namespace)
        FileUtils.mkdir_p(dir_path)

        serial_scheduler     = CacheBox::Scheduler::Serial.new
        concurrent_scheduler = CacheBox::Scheduler::Concurrent.new

        checks.call(file_storage, serial_scheduler)
        checks.call(file_storage, concurrent_scheduler)
      end

      memory_storage = CacheBox::Storage::Memory.new

      serial_scheduler     = CacheBox::Scheduler::Serial.new
      concurrent_scheduler = CacheBox::Scheduler::Concurrent.new

      checks.call(memory_storage, serial_scheduler)
      checks.call(memory_storage, concurrent_scheduler)
    end
  end
end
