# frozen_string_literal: true

require 'test_helper'

describe CacheBox::Scheduler::Serial do
  let(:described_class) { CacheBox::Scheduler::Serial }
  let(:root) { File.join(Dir.pwd, 'test', '.temp') }
  let(:namespace) { 'namespace' }

  describe '#run!' do
    it 'runs the graph in the right order' do
      checks = proc do |storage|
        subject = described_class.new

        subject.add(:a)
        subject.add(:c)
        subject.add(:b)
        subject.add(d: :a)
        subject.add(e: :b)
        subject.add(f: :d)
        subject.add(g: :e)

        effects = []

        unit  = CacheBox::Unit.new(namespace, storage: storage)
        nodes = {
          'a' => ['a', proc { unit.with(:a) { effects << :a; 1 } }],
          'b' => ['b', proc { unit.with(:b) { effects << :b; 2 } }],
          'c' => ['c', proc { unit.with(:c) { effects << :c; 3 } }],
          'd' => ['d', proc { unit.with(:d) { effects << :d; 4 } }],
          'e' => ['e', proc { unit.with(:e) { effects << :e; 5 } }],
          'f' => ['f', proc { unit.with(:f) { effects << :f; 6 } }],
          'g' => ['g', proc { unit.with(:g) { effects << :g; 7 } }]
        }

        expected = { c: 3, f: 6, g: 7 }
        result   = subject.run!(nodes, unit)
        assert_equal(expected, result)

        expected = [:a, :b, :d, :e, :c, :f, :g]
        assert_equal(expected, effects)
      end

      Dir.mktmpdir(nil, root) do |dir|
        file_storage = CacheBox::Storage::File.new(namespace: namespace, path: dir)

        dir_path = File.join(dir, namespace)
        FileUtils.mkdir_p(dir_path)

        checks.call(file_storage)
      end

      memory_storage = CacheBox::Storage::Memory.new
      checks.call(memory_storage)
    end

    it 'does not run already cached jobs' do
      checks = proc do |storage|
        subject = described_class.new

        subject.add(:a)
        subject.add(:c)
        subject.add(:b)
        subject.add(d: :a)
        subject.add(e: :b)
        subject.add(f: :d)
        subject.add(g: :e)

        effects = []
        error   = true

        unit  = CacheBox::Unit.new(namespace, storage: storage)
        nodes = {
          'a' => ['a', proc { unit.with(:a) { effects << :a; 1 } }],
          'b' => ['b', proc { unit.with(:b) { effects << :b; 2 } }],
          'c' => ['c', proc { unit.with(:c) { effects << :c; 3 } }],
          'd' => ['d', proc { unit.with(:d) { effects << :d; 4 } }],

          'e' => ['e', proc do
                         unit.with(:e) do
                           effects << :e;
                           raise 'Oopsie!' if error

                           5
                         end
                       end],

          'f' => ['f', proc { unit.with(:f) { effects << :f; 6 } }],
          'g' => ['g', proc { unit.with(:g) { effects << :g; 7 } }]
        }

        error = assert_raises(StandardError) { subject.run!(nodes, unit) }
        assert_equal('Oopsie!', error.message)

        expected = [:a, :b, :d, :e]
        assert_equal(expected, effects)

        effects = []
        error   = false
        result  = subject.run!(nodes, unit)

        expected = [:e, :c, :f, :g]
        assert_equal(expected, effects)

        expected = { c: 3, f: 6, g: 7 }
        assert_equal(expected, result)
      end

      Dir.mktmpdir(nil, root) do |dir|
        file_storage = CacheBox::Storage::File.new(namespace: namespace, path: dir)

        dir_path = File.join(dir, namespace)
        FileUtils.mkdir_p(dir_path)

        checks.call(file_storage)
      end

      memory_storage = CacheBox::Storage::Memory.new
      checks.call(memory_storage)
    end
  end
end
