# frozen_string_literal: true

require 'test_helper'

describe CacheBox::Chain do
  let(:described_class) { CacheBox::Chain }
  let(:root) { File.join(Dir.pwd, 'test', '.temp') }

  describe '#expire!' do
    it 'expires the cache' do
      checks = proc do |storage|
        subject = described_class.new(storage: storage)

        assert_equal(false, storage.has?('key'))
        subject.add(:key) { 42 }
        subject.run!
        assert_equal(true, storage.has?('key'))

        subject.expire!(:key)
        assert_equal(false, storage.has?('key'))

        assert_equal(false, storage.has?('key_1'))
        assert_equal(false, storage.has?('key_2'))
        subject.add(:key_1) { 42 }
        subject.add(:key_2) { 24 }
        subject.run!
        assert_equal(true, storage.has?('key_1'))
        assert_equal(true, storage.has?('key_2'))

        subject.expire!
        assert_equal(false, storage.has?('key_1'))
        assert_equal(false, storage.has?('key_2'))
      end

      Dir.mktmpdir(nil, root) do |dir|
        namespace    = 'namespace'
        file_storage = CacheBox::Storage::File.new(namespace: namespace, path: dir)

        dir_path = File.join(dir, namespace)
        FileUtils.mkdir_p(dir_path)

        checks.call(file_storage)
      end

      memory_storage = CacheBox::Storage::Memory.new
      checks.call(memory_storage)
    end
  end

  describe '#run!' do
    it 'recovers from failure without loading obsolete steps' do
      checks = proc do |storage|
        subject = described_class.new(storage: storage)
        effect  = []
        crash   = true

        subject.add(:first, [1, 2]) do |box|
          assert_equal([1, 2], box.args)
          assert_nil(box.input)

          effect << :first_effect

          42
        end
        subject.add(:second, [3, 4]) do |box|
          assert_equal([3, 4], box.args)
          assert_equal(42,     box.input)

          effect << :second_effect

          10
        end
        subject.add(:third) do |box|
          assert_nil(box.args)
          assert_equal(10,  box.input)

          raise 'Crashing!' if crash

          effect << :third_effect

          box.input + 100
        end

        error = assert_raises(StandardError) { subject.run! }
        assert_equal('Crashing!', error.message)
        assert_equal([:first_effect, :second_effect], effect)

        effect.clear
        crash  = false
        result = subject.run!
        assert_equal(110, result)
        assert_equal([:third_effect], effect)
      end

      Dir.mktmpdir(nil, root) do |dir|
        namespace    = 'namespace'
        file_storage = CacheBox::Storage::File.new(namespace: namespace, path: dir)

        dir_path = File.join(dir, namespace)
        FileUtils.mkdir_p(dir_path)

        checks.call(file_storage)
      end

      memory_storage = CacheBox::Storage::Memory.new
      checks.call(memory_storage)
    end

    it 'holds progress in stash for later reuse' do
      checks = proc do |storage|
        subject = described_class.new(storage: storage)
        effect  = []

        subject.add(:first) do
          effect << :first_effect

          42
        end
        subject.add(:second, [1, 2]) do |box|
          assert_equal([1, 2], box.args)
          assert_equal(42,     box.input)

          result = nil
          if box.stash.key?(:key)
            result = box.stash[:key]

            effect << :second_effect_result
          else
            box.stash[:key] = box.input
            effect << :second_effect_stash

            raise 'Crashing!'
          end

          result
        end
        subject.add(:third) do |box|
          assert_equal(42, box.input)

          effect << :third_effect

          box.input + 100
        end

        error = assert_raises(StandardError) { subject.run! }
        assert_equal('Crashing!', error.message)
        assert_equal([:first_effect, :second_effect_stash], effect)

        effect.clear
        result = subject.run!
        assert_equal(142, result)
        assert_equal([:second_effect_result, :third_effect], effect)
      end

      Dir.mktmpdir(nil, root) do |dir|
        namespace    = 'namespace'
        file_storage = CacheBox::Storage::File.new(namespace: namespace, path: dir)

        dir_path = File.join(dir, namespace)
        FileUtils.mkdir_p(dir_path)

        checks.call(file_storage)
      end

      memory_storage = CacheBox::Storage::Memory.new
      checks.call(memory_storage)
    end
  end
end
