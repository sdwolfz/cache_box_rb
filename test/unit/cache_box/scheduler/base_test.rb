# frozen_string_literal: true

require 'test_helper'

describe CacheBox::Scheduler::Base do
  let(:described_class) { CacheBox::Scheduler::Base }

  describe '#initialize' do
    it 'does not require arguments' do
      cache = described_class.new
      assert(cache.is_a?(described_class))

      cache = described_class.new(logger: nil)
      assert(cache.is_a?(described_class))
    end
  end

  describe '#add' do
    let(:subject) { described_class.new }

    it 'adds a proper edge to the scheduler plan' do
      result = subject.add('string')
      assert_same(subject, result)

      result = subject.add(:symbol)
      assert_same(subject, result)

      result = subject.add({ other_symbol: :symbol })
      assert_same(subject, result)

      result = subject.add({ 'other_string' => :symbol })
      assert_same(subject, result)

      result = subject.add({ another_symbol: 'string' })
      assert_same(subject, result)

      result = subject.add({ 'another_string' => 'string' })
      assert_same(subject, result)

      result = subject.add({ new_name: [:symbol] })
      assert_same(subject, result)

      result = subject.add({ other_name: ['string'] })
      assert_same(subject, result)

      result = subject.add({ another_name: [:symbol, 'string'] })
      assert_same(subject, result)

      result = subject.add(name: [:symbol, 'string'])
      assert_same(subject, result)
    end

    it 'raises when no proper edge provided' do
      error = assert_raises(ArgumentError) do
        subject.add(1)
      end

      message = 'edge has an unknown structure; Integer: 1'
      assert_equal(message, error.message)
    end

    it 'raises when hash edge does not have one key' do
      error = assert_raises(ArgumentError) do
        subject.add({})
      end

      message = 'edge must have one mapping, got 0: {}'
      assert_equal(message, error.message)

      error = assert_raises(ArgumentError) do
        subject.add({ key_1: :first, key_2: :second })
      end

      message = 'edge must have one mapping, got 2: {:key_1=>:first, :key_2=>:second}'
      assert_equal(message, error.message)
    end

    it 'raises when edge name is not a String nor Symbol' do
      error = assert_raises(ArgumentError) do
        subject.add({ 1 => :a })
      end

      message = 'edge name must be a Symbol or String, got Integer: 1'
      assert_equal(message, error.message)
    end

    it 'raises when edge needs is not a proper object' do
      error = assert_raises(ArgumentError) do
        subject.add({ a: 1 })
      end

      message = 'edge needs has an unknown structure; Integer: 1'
      assert_equal(message, error.message)

      error = assert_raises(ArgumentError) do
        subject.add({ a: [1] })
      end

      message = 'edge needs must contain Symbol or String, got Integer: 1 in [1]'
      assert_equal(message, error.message)
    end

    it 'raises when graph does not contain referenced node' do
      error = assert_raises(ArgumentError) do
        subject.add({ a: :b })
      end

      message = 'Graph does not contain required node :b'
      assert_equal(message, error.message)
    end

    it 'raises when graph already contains a node' do
      subject.add(:a)
      error = assert_raises(ArgumentError) do
        subject.add(:a)
      end

      message = 'Graph already contains a node named :a'
      assert_equal(message, error.message)
    end
  end

  describe '#run!' do
    let(:subject) { described_class.new }

    it 'raises a method not implemented error' do
      error = assert_raises(NotImplementedError) do
        subject.run!(nil, nil)
      end

      message = 'The `#run/2` method needs to be implemented in your subclass!'
      assert_equal(message, error.message)
    end
  end
end
