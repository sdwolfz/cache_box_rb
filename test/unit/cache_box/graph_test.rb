# frozen_string_literal: true

require 'test_helper'

describe CacheBox::Graph do
  let(:described_class) { CacheBox::Graph }

  describe '#initialize' do
    it 'does not require arguments' do
      cache = described_class.new
      assert(cache.is_a?(described_class))

      cache = described_class.new(nil)
      assert(cache.is_a?(described_class))

      cache = described_class.new(scheduler: nil)
      assert(cache.is_a?(described_class))

      cache = described_class.new(storage: nil)
      assert(cache.is_a?(described_class))

      cache = described_class.new(logger: nil)
      assert(cache.is_a?(described_class))
    end
  end

  describe '#node' do
    let(:mock) { Minitest::Mock.new }
    let(:subject) { described_class.new(storage: mock) }

    it 'raises when no proper edge provided' do
      error = assert_raises(ArgumentError) do
        subject.node(1)
      end

      message = 'edge has an unknown structure; Integer: 1'
      assert_equal(message, error.message)
    end

    it 'raises when hash edge does not have one key' do
      error = assert_raises(ArgumentError) do
        subject.node({})
      end

      message = 'edge must have one mapping, got 0: {}'
      assert_equal(message, error.message)

      error = assert_raises(ArgumentError) do
        subject.node({ key_1: :first, key_2: :second })
      end

      message = 'edge must have one mapping, got 2: {:key_1=>:first, :key_2=>:second}'
      assert_equal(message, error.message)
    end

    it 'raises when edge name is not a String nor Symbol' do
      error = assert_raises(ArgumentError) do
        subject.node({ 1 => :a })
      end

      message = 'edge name must be a Symbol or String, got Integer: 1'
      assert_equal(message, error.message)
    end

    it 'raises when edge needs is not a proper object' do
      error = assert_raises(ArgumentError) do
        subject.node({ a: 1 })
      end

      message = 'edge needs has an unknown structure; Integer: 1'
      assert_equal(message, error.message)

      error = assert_raises(ArgumentError) do
        subject.node({ a: [1] })
      end

      message = 'edge needs must contain Symbol or String, got Integer: 1 in [1]'
      assert_equal(message, error.message)
    end

    it 'raises when graph does not contain referenced node' do
      error = assert_raises(ArgumentError) do
        subject.node({ a: :b }) { 1 }
      end

      message = 'Graph does not contain required node :b'
      assert_equal(message, error.message)
    end

    it 'raises when graph already contains a node' do
      subject.node(:a) { 1 }
      error = assert_raises(ArgumentError) do
        subject.node(:a) { 1 }
      end

      message = 'Graph already contains a node named :a'
      assert_equal(message, error.message)
    end

    it 'raises when no block given' do
      mock    = Minitest::Mock.new
      subject = described_class.new(storage: mock)

      error = assert_raises(ArgumentError) do
        subject.node('name')
      end

      message = 'The `#node` method requires a block'
      assert_equal(message, error.message)
    end
  end
end
