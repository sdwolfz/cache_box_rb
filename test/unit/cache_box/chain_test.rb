# frozen_string_literal: true

require 'test_helper'

describe CacheBox::Chain do
  let(:described_class) { CacheBox::Chain }

  describe '#initialize' do
    it 'does not require arguments' do
      cache = described_class.new
      assert(cache.is_a?(described_class))

      cache = described_class.new(nil)
      assert(cache.is_a?(described_class))

      cache = described_class.new(logger: nil)
      assert(cache.is_a?(described_class))

      cache = described_class.new(storage: nil)
      assert(cache.is_a?(described_class))
    end
  end

  describe '#add' do
    it 'raises when given name is not a Symbol nor String' do
      mock    = Minitest::Mock.new
      subject = described_class.new(storage: mock)

      error = assert_raises(ArgumentError) do
        subject.add(1)
      end

      message = 'name must be a Symbol or String, got Integer: 1'
      assert_equal(message, error.message)
    end

    it 'raises when no block given' do
      mock    = Minitest::Mock.new
      subject = described_class.new(storage: mock)

      error = assert_raises(ArgumentError) do
        subject.add('name')
      end

      message = 'The `#add` method requires a block'
      assert_equal(message, error.message)
    end

    it 'raises when given the same name twice' do
      mock    = Minitest::Mock.new
      subject = described_class.new(storage: mock)

      subject.add(:name) { 42 }
      error = assert_raises(ArgumentError) do
        subject.add(:name) { 43 }
      end

      message = 'Chain already contains a step named :name'
      assert_equal(message, error.message)
    end
  end
end
