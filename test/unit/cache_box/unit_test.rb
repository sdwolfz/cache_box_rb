# frozen_string_literal: true

require 'test_helper'

describe CacheBox::Unit do
  let(:described_class) { CacheBox::Unit }

  describe '#initialize' do
    it 'does not require arguments' do
      cache = described_class.new
      assert(cache.is_a?(described_class))

      cache = described_class.new(nil)
      assert(cache.is_a?(described_class))

      cache = described_class.new(logger: nil)
      assert(cache.is_a?(described_class))

      cache = described_class.new(storage: nil)
      assert(cache.is_a?(described_class))
    end

    it 'raises when gives namespace is not a String nor Symbol' do
      error = assert_raises(ArgumentError) do
        described_class.new(1)
      end

      message = 'namespace must be a Symbol or String, got Integer: 1'
      assert_equal(message, error.message)
    end
  end

  describe '#with' do
    it 'returns existing state from storage without calling given block' do
      mock = Minitest::Mock.new
      mock.expect(:read!, { result: 1 }, ['first'])
      mock.expect(:read!, { result: 2 }, ['second'])

      subject = described_class.new(storage: mock)

      result = subject.with(:first) { 42 }
      assert_equal(1, result)

      result = subject.with(:second) { 43 }
      assert_equal(2, result)

      result = subject.with(:first) { 44 }
      assert_equal(1, result)

      mock.verify
    end

    it 'writes value returned by block to storage' do
      mock = Minitest::Mock.new
      mock.expect(:read!,  nil, ['first'])
      mock.expect(:write!, nil, ['first', { result: 42 }])

      subject = described_class.new(storage: mock)

      result = subject.with(:first) { 42 }
      assert_equal(42, result)

      mock.verify
    end

    it 'writes stash to storage when block fails to run successfully' do
      mock = Minitest::Mock.new
      mock.expect(:read!,  nil, ['first'])
      mock.expect(:write!, nil, ['first', { stash: { 1 => 42 } }])

      subject = described_class.new(storage: mock)

      error = assert_raises(RuntimeError) do
        subject.with(:first) do |box|
          box.stash[1] = 42

          raise 'ERROR!!!'
        end
      end

      message = 'ERROR!!!'
      assert_equal(message, error.message)

      mock.verify
    end

    it 'passes extra arguments to block' do
      mock = Minitest::Mock.new
      mock.expect(:read!,  nil, ['first'])
      mock.expect(:write!, nil, ['first', { result: 42 }])

      subject = described_class.new(storage: mock)
      result  = subject.with(:first, [1, 2], '3') do |box|
        assert_equal([1, 2], box.args)
        assert_equal('3',    box.input)

        42
      end

      assert_equal(result, 42)
      mock.verify
    end

    it 'raises when given name is not a Symbol nor String' do
      mock    = Minitest::Mock.new
      subject = described_class.new(storage: mock)

      error = assert_raises(ArgumentError) do
        subject.with(1)
      end

      message = 'name must be a Symbol or String, got Integer: 1'
      assert_equal(message, error.message)
    end

    it 'raises when no block given' do
      mock    = Minitest::Mock.new
      subject = described_class.new(storage: mock)

      error = assert_raises(ArgumentError) do
        subject.with
      end

      message = 'The `#with` method requires a block'
      assert_equal(message, error.message)
    end
  end

  describe '#has?' do
    it 'defaults to "name" when no argument is given' do
      mock = Minitest::Mock.new
      mock.expect(:read!, {}, ['name'])

      subject = described_class.new(storage: mock)
      result  = subject.has?

      assert_same(false, result)
      mock.verify
    end

    it 'reads given name from the storage' do
      mock = Minitest::Mock.new
      mock.expect(:read!, {}, ['first'])

      subject = described_class.new(storage: mock)
      result  = subject.has?(:first)

      assert_same(false, result)
      mock.verify
    end

    it 'checks given name is in storage' do
      mock = Minitest::Mock.new
      mock.expect(:read!, { result: '1' }, ['first'])

      subject = described_class.new(storage: mock)
      result  = subject.has?(:first)

      assert_same(true, result)
      mock.verify

      mock = Minitest::Mock.new
      mock.expect(:read!, { stash: '1' }, ['first'])

      subject = described_class.new(storage: mock)
      result  = subject.has?(:first)

      assert_same(false, result)
      mock.verify
    end

    it 'raises when given name is not a Symbol nor String' do
      subject = described_class.new

      error = assert_raises(ArgumentError) do
        subject.has?(1)
      end

      message = 'name must be a Symbol or String, got Integer: 1'
      assert_equal(message, error.message)
    end
  end

  describe '#result' do
    it 'defaults to "name" when no argument is given' do
      mock = Minitest::Mock.new
      mock.expect(:read!, {}, ['name'])

      subject = described_class.new(storage: mock)
      result  = subject.result

      assert_same(nil, result)
      mock.verify
    end

    it 'reads given name from the storage' do
      mock = Minitest::Mock.new
      mock.expect(:read!, {}, ['first'])

      subject = described_class.new(storage: mock)
      result  = subject.result(:first)

      assert_same(nil, result)
      mock.verify
    end

    it 'checks given name is in storage' do
      mock = Minitest::Mock.new
      mock.expect(:read!, { result: '1' }, ['first'])

      subject = described_class.new(storage: mock)
      result  = subject.result(:first)

      assert_equal('1', result)
      mock.verify

      mock = Minitest::Mock.new
      mock.expect(:read!, { stash: '1' }, ['first'])

      subject = described_class.new(storage: mock)
      result  = subject.result(:first)

      assert_same(nil, result)
      mock.verify
    end

    it 'raises when given name is not a Symbol nor String' do
      subject = described_class.new

      error = assert_raises(ArgumentError) do
        subject.result(1)
      end

      message = 'name must be a Symbol or String, got Integer: 1'
      assert_equal(message, error.message)
    end
  end

  describe '#expire!' do
    it 'expires the entire cache when no argument is given' do
      mock = Minitest::Mock.new
      mock.expect(:reset!, nil)

      subject = described_class.new(storage: mock)
      result  = subject.expire!

      assert(result.is_a?(described_class))
      mock.verify
    end

    it 'expires the specific name when given as argument' do
      mock = Minitest::Mock.new
      mock.expect(:delete!, nil, ['first'])
      mock.expect(:delete!, nil, ['second'])

      subject = described_class.new(storage: mock)
      result  = subject.expire!(:first, :second)

      assert(result.is_a?(described_class))
      mock.verify
    end

    it 'raises when given name is not a Symbol nor String' do
      subject = described_class.new

      error = assert_raises(ArgumentError) do
        subject.expire!(1)
      end

      message = 'names must contain Symbol or String, got Integer: 1 in [1]'
      assert_equal(message, error.message)

      error = assert_raises(ArgumentError) do
        subject.expire!(:first, 2)
      end

      message = 'names must contain Symbol or String, got Integer: 2 in [:first, 2]'
      assert_equal(message, error.message)
    end
  end

  describe '#clear' do
    it 'clears the entire state when no argument is given' do
      mock = Minitest::Mock.new
      mock.expect(:read!, { result: 1 }, ['a'])
      mock.expect(:read!, { result: 2 }, ['b'])
      mock.expect(:read!, { result: 3 }, ['c'])
      mock.expect(:read!, { result: 4 }, ['d'])
      mock.expect(:read!, { result: 5 }, ['e'])
      mock.expect(:read!, { result: 6 }, ['f'])

      subject = described_class.new(storage: mock)

      assert_equal(1, subject.with(:a) { 1 })
      assert_equal(2, subject.with(:b) { 2 })
      assert_equal(3, subject.with(:c) { 3 })
      assert_equal(4, subject.with(:d) { 4 })
      assert_equal(5, subject.with(:e) { 5 })
      assert_equal(6, subject.with(:f) { 6 })

      mock.verify

      result = subject.clear(:a)
      assert(result.is_a?(described_class))

      mock.expect(:read!, { result: 1 }, ['a'])
      assert_equal(1, subject.with(:a) { 1 })
      mock.verify

      subject.clear(:b, :c)
      assert(result.is_a?(described_class))

      mock.expect(:read!, { result: 2 }, ['b'])
      mock.expect(:read!, { result: 3 }, ['c'])
      assert_equal(2, subject.with(:b) { 2 })
      assert_equal(3, subject.with(:c) { 3 })
      mock.verify

      subject.clear
      assert(result.is_a?(described_class))

      mock.expect(:read!, { result: 6 }, ['f'])
      assert_equal(6, subject.with(:f) { 6 })
    end

    it 'expires the specific name when given as argument' do
      mock = Minitest::Mock.new

      subject = described_class.new(storage: mock)
      result  = subject.clear(:first, :second)

      assert(result.is_a?(described_class))
    end

    it 'raises when given name is not a Symbol nor String' do
      subject = described_class.new

      error = assert_raises(ArgumentError) do
        subject.clear(1)
      end

      message = 'names must contain Symbol or String, got Integer: 1 in [1]'
      assert_equal(message, error.message)

      error = assert_raises(ArgumentError) do
        subject.clear(:first, 2)
      end

      message = 'names must contain Symbol or String, got Integer: 2 in [:first, 2]'
      assert_equal(message, error.message)
    end
  end
end
