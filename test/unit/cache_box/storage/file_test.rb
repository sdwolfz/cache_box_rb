# frozen_string_literal: true

require 'test_helper'

describe CacheBox::Storage::File do
  let(:described_class) { CacheBox::Storage::File }

  describe '#initialize' do
    it 'does not require arguments' do
      cache = described_class.new
      assert(cache.is_a?(described_class))

      cache = described_class.new(namespace: nil)
      assert(cache.is_a?(described_class))

      cache = described_class.new(path: nil)
      assert(cache.is_a?(described_class))

      cache = described_class.new(namespace: nil, path: nil)
      assert(cache.is_a?(described_class))
    end

    it 'raises when gives namespace is not a String nor Symbol' do
      namespace = 1
      error     = assert_raises(ArgumentError) do
        described_class.new(namespace: namespace)
      end

      message = 'namespace must be a Symbol or String, got Integer: 1'
      assert_equal(message, error.message)
    end

    it 'raises when gives path is not a String' do
      path  = 1
      error = assert_raises(ArgumentError) do
        described_class.new(path: path)
      end

      message = 'path must be a String, got Integer: 1'
      assert_equal(message, error.message)
    end
  end

  describe '#reset!' do
    let(:namespace) { 'namespace' }
    let(:root) { File.join(Dir.pwd, 'test', '.temp') }

    it 'removes the state file' do
      Dir.mktmpdir(nil, root) do |dir|
        subject = described_class.new(namespace: namespace, path: dir)

        dir_path = File.join(dir, namespace)
        FileUtils.mkdir_p(dir_path)

        file_path = File.join(dir_path, 'name')
        File.write(file_path, '1')
        assert(File.exist?(file_path))

        result = subject.reset!

        assert(result.is_a?(described_class))
        assert(!File.exist?(file_path))
      end
    end
  end

  describe '#read!' do
    let(:namespace) { 'namespace' }
    let(:file_name) { 'name' }
    let(:root) { File.join(Dir.pwd, 'test', '.temp') }

    it 'reads the content of a state file' do
      Dir.mktmpdir(nil, root) do |dir|
        subject = described_class.new(namespace: namespace, path: dir)

        dir_path = File.join(dir, namespace)
        FileUtils.mkdir_p(dir_path)

        file_path = File.join(dir_path, file_name)
        File.write(file_path, Marshal.dump(1))
        assert(File.exist?(file_path))

        result = subject.read!(file_name)
        assert_equal(1, result)
      end
    end

    it 'raises when gives path is not a String' do
      path  = 1
      error = assert_raises(ArgumentError) do
        Dir.mktmpdir(nil, root) do |dir|
          subject = described_class.new(namespace: namespace, path: dir)

          subject.read!(path)
        end
      end

      message = 'name must be a String, got Integer: 1'
      assert_equal(message, error.message)
    end
  end

  describe '#write!' do
    let(:namespace) { 'namespace' }
    let(:file_name) { 'name' }
    let(:root) { File.join(Dir.pwd, 'test', '.temp') }

    it 'reads the content of a state file' do
      Dir.mktmpdir(nil, root) do |dir|
        subject = described_class.new(namespace: namespace, path: dir)

        dir_path = File.join(dir, namespace)
        FileUtils.mkdir_p(dir_path)

        file_path = File.join(dir_path, file_name)
        assert(!File.exist?(file_path))

        result = subject.write!(file_name, 1)
        assert(result.is_a?(described_class))
        assert(File.exist?(file_path))

        result = Marshal.load(File.read(file_path))
        assert_equal(1, result)
      end
    end

    it 'raises when gives path is not a String' do
      path  = 1
      error = assert_raises(ArgumentError) do
        Dir.mktmpdir(nil, root) do |dir|
          subject = described_class.new(namespace: namespace, path: dir)

          subject.write!(path, 2)
        end
      end

      message = 'name must be a String, got Integer: 1'
      assert_equal(message, error.message)
    end
  end

  describe '#delete!' do
    let(:namespace) { 'namespace' }
    let(:file_name) { 'name' }
    let(:root) { File.join(Dir.pwd, 'test', '.temp') }

    it 'reads the content of a state file' do
      Dir.mktmpdir(nil, root) do |dir|
        subject = described_class.new(namespace: namespace, path: dir)

        dir_path = File.join(dir, namespace)
        FileUtils.mkdir_p(dir_path)

        file_path = File.join(dir_path, file_name)
        File.write(file_path, 1)
        assert(File.exist?(file_path))

        result = subject.delete!(file_name)
        assert(result.is_a?(described_class))
        assert(!File.exist?(file_path))
      end
    end

    it 'raises when gives path is not a String nor Symbol' do
      path  = 1
      error = assert_raises(ArgumentError) do
        Dir.mktmpdir(nil, root) do |dir|
          subject = described_class.new(namespace: namespace, path: dir)

          subject.delete!(path)
        end
      end

      message = 'name must be a String, got Integer: 1'
      assert_equal(message, error.message)
    end
  end

  describe '#has?' do
    let(:namespace) { 'namespace' }
    let(:file_name) { 'name' }
    let(:root) { File.join(Dir.pwd, 'test', '.temp') }

    it 'checks the existance of a file' do
      Dir.mktmpdir(nil, root) do |dir|
        subject = described_class.new(namespace: namespace, path: dir)

        dir_path = File.join(dir, namespace)
        FileUtils.mkdir_p(dir_path)

        file_path = File.join(dir_path, file_name)
        File.write(file_path, 1)
        assert(File.exist?(file_path))

        result = subject.has?(file_name)
        assert_equal(true, result)

        file_path = File.join(dir_path, 'nonexisting')
        assert(!File.exist?(file_path))

        result = subject.has?('nonexisting')
        assert_equal(false, result)
      end
    end

    it 'raises when gives path is not a String nor Symbol' do
      path  = 1
      error = assert_raises(ArgumentError) do
        Dir.mktmpdir(nil, root) do |dir|
          subject = described_class.new(namespace: namespace, path: dir)

          subject.has?(path)
        end
      end

      message = 'name must be a String, got Integer: 1'
      assert_equal(message, error.message)
    end
  end
end
