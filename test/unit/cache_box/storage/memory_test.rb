# frozen_string_literal: true

require 'test_helper'

describe CacheBox::Storage::Memory do
  let(:described_class) { CacheBox::Storage::Memory }

  describe '#initialize' do
    it 'does not require arguments' do
      cache = described_class.new
      assert(cache.is_a?(described_class))

      cache = described_class.new(nil)
      assert(cache.is_a?(described_class))
    end

    it 'can be given a Hash as argument' do
      cache = described_class.new({})

      assert(cache.is_a?(described_class))
    end

    it 'can be given a Hash like object as argument' do
      klass = Class.new do
        def [](_k);      end
        def []=(_k, _v); end
        def delete(_k);  end
        def key?(_k);    end
      end
      state = klass.new
      cache = described_class.new(state)

      assert(cache.is_a?(described_class))
    end

    it 'raises when gives state does not respond to `:[]`' do
      state = Class.new.new
      error = assert_raises(ArgumentError) { described_class.new(state) }

      message = 'Given state object does not respond to `:[]`'
      assert_equal(message, error.message)
    end

    it 'raises when the arity of `:[]` is not 1' do
      klass = Class.new do
        def [](_k, _w); end
      end
      state = klass.new

      error = assert_raises(ArgumentError) { described_class.new(state) }

      message = "Given state object's `:[]` method arity must be 1, got: 2"
      assert_equal(message, error.message)
    end

    it 'raises when gives state does not respond to `:[]=`' do
      klass = Class.new do
        def [](_k); end
      end
      state = klass.new
      error = assert_raises(ArgumentError) { described_class.new(state) }

      message = 'Given state object does not respond to `:[]=`'
      assert_equal(message, error.message)
    end

    it 'raises when the arity of `:[]=` is not 2' do
      klass = Class.new do
        def [](_k);  end
        def []=(_w); end
      end
      state = klass.new

      error = assert_raises(ArgumentError) { described_class.new(state) }

      message = "Given state object's `:[]=` method arity must be 2, got: 1"
      assert_equal(message, error.message)
    end

    it 'raises when gives state does not respond to `delete`' do
      klass = Class.new do
        def [](_k);      end
        def []=(_k, _v); end
      end
      state = klass.new
      error = assert_raises(ArgumentError) { described_class.new(state) }

      message = 'Given state object does not respond to `delete`'
      assert_equal(message, error.message)
    end

    it 'raises when the arity of `detete` is not 1' do
      klass = Class.new do
        def [](_k);         end
        def []=(_k, _v);    end
        def delete(_m, _w); end
      end
      state = klass.new

      error = assert_raises(ArgumentError) { described_class.new(state) }

      message = "Given state object's `:delete` method arity must be 1, got: 2"
      assert_equal(message, error.message)
    end

    it 'raises when gives state does not respond to `key?`' do
      klass = Class.new do
        def [](_k);      end
        def []=(_k, _v); end
        def delete(_k);  end
      end
      state = klass.new
      error = assert_raises(ArgumentError) { described_class.new(state) }

      message = 'Given state object does not respond to `:key?`'
      assert_equal(message, error.message)
    end

    it 'raises when the arity of `key` is not 1' do
      klass = Class.new do
        def [](_k);       end
        def []=(_k, _v);  end
        def delete(_k);   end
        def key?(_m, _w); end
      end
      state = klass.new

      error = assert_raises(ArgumentError) { described_class.new(state) }

      message = "Given state object's `:key?` method arity must be 1, got: 2"
      assert_equal(message, error.message)
    end
  end

  describe '#reset!' do
    let(:initial_state) do
      state = Minitest::Mock.new
      state.expect(:nil?, false)
      state.expect(:is_a?, true, [Hash])

      state
    end

    let(:final_state) do
      state = Minitest::Mock.new
      state.expect(:nil?, false)
      state.expect(:is_a?, true, [Hash])

      state
    end

    let(:subject) { described_class.new(initial_state) }

    it 'resets the state' do
      first_result = subject.reset!

      assert(first_result.is_a?(described_class))
      initial_state.verify

      second_result = subject.reset!(final_state)

      assert(second_result.is_a?(described_class))
      final_state.verify
    end
  end

  describe '#read!' do
    let(:state) do
      state = Minitest::Mock.new
      state.expect(:nil?, false)
      state.expect(:is_a?, true, [Hash])

      state.expect(:[], 'value', ['name'])

      state
    end
    let(:subject) { described_class.new(state) }

    it 'returns an item from the state' do
      expected = 'value'
      result   = subject.read!('name')

      assert_equal(expected, result)
      state.verify
    end

    it 'raises when gives name is not a String' do
      error = assert_raises(ArgumentError) do
        described_class.new(state).read!(1)
      end

      message = 'name must be a String, got Integer: 1'
      assert_equal(message, error.message)
    end
  end

  describe '#write!' do
    let(:state) do
      state = Minitest::Mock.new
      state.expect(:nil?, false)
      state.expect(:is_a?, true, [Hash])

      state.expect(:[]=, nil, ['name', 'value'])

      state
    end
    let(:subject) { described_class.new(state) }

    it 'adds an item to the state' do
      result = subject.write!('name', 'value')

      assert(result.is_a?(described_class))
      state.verify
    end

    it 'raises when gives name is not a String' do
      error = assert_raises(ArgumentError) do
        described_class.new(state).write!(1, 2)
      end

      message = 'name must be a String, got Integer: 1'
      assert_equal(message, error.message)
    end
  end

  describe '#delete!' do
    let(:state) do
      state = Minitest::Mock.new
      state.expect(:nil?, false)
      state.expect(:is_a?, true, [Hash])

      state.expect(:delete, nil, ['name'])

      state
    end
    let(:subject) { described_class.new(state) }

    it 'deletes an item from the state' do
      result = subject.delete!('name')

      assert(result.is_a?(described_class))
      state.verify
    end

    it 'raises when gives name is not a String' do
      error = assert_raises(ArgumentError) do
        described_class.new(state).delete!(1)
      end

      message = 'name must be a String, got Integer: 1'
      assert_equal(message, error.message)
    end
  end

  describe '#has?' do
    let(:state) do
      state = Minitest::Mock.new
      state.expect(:nil?, false)
      state.expect(:is_a?, true, [Hash])

      state.expect(:key?, true, ['name'])

      state
    end
    let(:subject) { described_class.new(state) }

    it 'checks the existance of a key' do
      expected = true
      result   = subject.has?('name')

      assert_equal(expected, result)
      state.verify
    end

    it 'raises when gives name is not a String' do
      error = assert_raises(ArgumentError) do
        described_class.new(state).has?(1)
      end

      message = 'name must be a String, got Integer: 1'
      assert_equal(message, error.message)
    end
  end
end
