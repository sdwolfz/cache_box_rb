# frozen_string_literal: true

require 'test_helper'

describe CacheBox::Helper::Validate do
  let(:klass) do
    Class.new do
      include CacheBox::Helper::Validate
    end
  end
  let(:subject) { klass.new }

  describe '#validate_string_or_symbol!' do
    it 'returns the original object when String or Symbol' do
      expected = 'string'
      result   = subject.validate_string_or_symbol!(expected, 'name')
      assert_same(expected, result)

      expected = :symbol
      result   = subject.validate_string_or_symbol!(expected, 'name')
      assert_same(expected, result)
    end

    it 'raises when no compatible object provided' do
      error = assert_raises(ArgumentError) do
        subject.validate_string_or_symbol!(1, 'name')
      end

      message = 'name must be a Symbol or String, got Integer: 1'
      assert_equal(message, error.message)
    end
  end

  describe '#validate_array_of_string_or_symbol!' do
    it 'returns the original object when a proper input is provided' do
      expected = ['string', :symbol]
      result   = subject.validate_array_of_string_or_symbol!(expected, 'name')

      assert_same(expected, result)
    end

    it 'raises when not provided an Array' do
      error = assert_raises(ArgumentError) do
        subject.validate_array_of_string_or_symbol!(1, 'name')
      end

      message = 'name must be an Array, got Integer: 1'
      assert_equal(message, error.message)
    end

    it 'raises when not provided String or Symbol elements' do
      error = assert_raises(ArgumentError) do
        subject.validate_array_of_string_or_symbol!([1], 'name')
      end

      message = 'name must contain Symbol or String, got Integer: 1 in [1]'
      assert_equal(message, error.message)
    end
  end

  describe '#validate_block_presence!' do
    it 'returns the original object when a proper input is provided' do
      expected = proc {}
      result   = subject.validate_block_presence!(expected, 'name')

      assert_same(expected, result)
    end

    it 'raises when not provided an input' do
      error = assert_raises(ArgumentError) do
        subject.validate_block_presence!(nil, 'name')
      end

      message = 'The `name` method requires a block'
      assert_equal(message, error.message)
    end
  end

  describe '#validate_edge!' do
    it 'returns the original object when a proper input is provided' do
      expected = 'string'
      result   = subject.validate_edge!(expected)
      assert_same(expected, result)

      expected = :symbol
      result   = subject.validate_edge!(expected)
      assert_same(expected, result)

      expected = { name: :other }
      result   = subject.validate_edge!(expected)
      assert_same(expected, result)

      expected = { 'name' => :other }
      result   = subject.validate_edge!(expected)
      assert_same(expected, result)

      expected = { name: 'other' }
      result   = subject.validate_edge!(expected)
      assert_same(expected, result)

      expected = { 'name' => 'other' }
      result   = subject.validate_edge!(expected)
      assert_same(expected, result)

      expected = { name: [:other] }
      result   = subject.validate_edge!(expected)
      assert_same(expected, result)

      expected = { name: ['other'] }
      result   = subject.validate_edge!(expected)
      assert_same(expected, result)

      expected = { name: [:other, 'another'] }
      result   = subject.validate_edge!(expected)
      assert_same(expected, result)

      expected = { name: [:other, 'another'] }
      result   = subject.validate_edge!(**expected)
      assert_equal(expected, result)
    end

    it 'raises when no proper edge provided' do
      error = assert_raises(ArgumentError) do
        subject.validate_edge!(1)
      end

      message = 'edge has an unknown structure; Integer: 1'
      assert_equal(message, error.message)
    end

    it 'raises when hash edge does not have one key' do
      error = assert_raises(ArgumentError) do
        subject.validate_edge!({})
      end

      message = 'edge must have one mapping, got 0: {}'
      assert_equal(message, error.message)

      error = assert_raises(ArgumentError) do
        subject.validate_edge!({ key_1: :first, key_2: :second })
      end

      message = 'edge must have one mapping, got 2: {:key_1=>:first, :key_2=>:second}'
      assert_equal(message, error.message)
    end

    it 'raises when edge name is not a String nor Symbol' do
      error = assert_raises(ArgumentError) do
        subject.validate_edge!({ 1 => :a })
      end

      message = 'edge name must be a Symbol or String, got Integer: 1'
      assert_equal(message, error.message)
    end

    it 'raises when edge needs is not a proper object' do
      error = assert_raises(ArgumentError) do
        subject.validate_edge!({ a: 1 })
      end

      message = 'edge needs has an unknown structure; Integer: 1'
      assert_equal(message, error.message)

      error = assert_raises(ArgumentError) do
        subject.validate_edge!({ a: [1] })
      end

      message = 'edge needs must contain Symbol or String, got Integer: 1 in [1]'
      assert_equal(message, error.message)
    end
  end
end
