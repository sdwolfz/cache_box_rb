#-------------------------------------------------------------------------------
# Settings
#-------------------------------------------------------------------------------

.DEFAULT_GOAL := help

#-------------------------------------------------------------------------------
# Goals
#-------------------------------------------------------------------------------

.PHONY: help
help:
	@echo 'Usage: make [<GOAL_1>, ...] [<VARIABLE_1>=value_1, ...]'
	@echo ''
	@echo 'Examples:'
	@echo '  make'
	@echo '  make help'
	@echo ''
	@echo 'Goals:'
	@echo '  - help: Display this help message'
	@echo ''
	@echo '  - console: Start a `pry` console'
	@echo '  - ci:      Run the entire CI setup using docker'
	@echo '  - gems:    Updates the `Gemfile.lock` (Development only)'
	@echo ''
	@echo '  - build: Build the docker image'
	@echo '  - shell: Run a `sh` shell in the docker image'
	@echo ''
	@echo '  - lint/eclint:   Run `eclint`'
	@echo '  - lint/hadolint: Run `hadolint`'
	@echo '  - lint/yamlliny: Run `yamllint`'
	@echo ''
	@echo 'Default Goal: help'
	@echo ''

#-------------------------------------------------------------------------------
# Convenience

RUBY_VERSION := '3.1.2'

.PHONY: console
console:
	@ruby ./bin/console

.PHONY: gems
gems:
	@$(DOCKER_HERE) ruby:$(RUBY_VERSION)-alpine bundle lock --update

#-------------------------------------------------------------------------------
# Docker

DOCKER_HERE := docker run --rm -it -u `id -u`:`id -g` -v "$(PWD)":/here -w /here
IMAGE_NAME  := cache-box

.PHONY: build
build:
	@docker build -t $(IMAGE_NAME) .

.PHONY: shell
shell:
	@$(DOCKER_HERE) $(IMAGE_NAME) sh

#-------------------------------------------------------------------------------
# CI

.PHONY: ci
ci:
	@$(DOCKER_HERE) sdwolfz/eclint:latest make lint/eclint
	@$(DOCKER_HERE) sdwolfz/hadolint:latest make lint/hadolint
	@$(DOCKER_HERE) sdwolfz/yamllint:latest make lint/yamllint

.PHONY: lint/eclint
lint/eclint:
	@eclint check $(git ls-files)

.PHONY: lint/hadolint
lint/hadolint:
	@find . -name Dockerfile | xargs hadolint

.PHONY: lint/yamllint
lint/yamllint:
	@yamllint .
