# Development

## Prerequisites

* `docker`
* `docker-here`
* `envchain`
* `make`

## Dependencies

Update the `Gemfile.lock` without installg gems by doing:

```sh
make gems
```

### Docker

Create the docker image:

```sh
make build
```

Start a shell:

```sh
make shell
```

### Gem

Build the gem using:

```sh
gem build cache_box.gemspec --output=release/cache_box.gem
```

### Console

```sh
make console
```

### Test

#### Unit

```sh
bundle exec rake test
```

#### Stress

```sh
bundle exec ruby stress/concurrent_scheduler_stress_test.rb
```

#### Lint

```sh
bundle exec rubocop
```

#### Local

Start a local shell:

```sh
make shell
```

Install from a release:

```sh
gem install ./release/cache_box.gem
```

Start `irb`:

```sh
irb -r cache_box
```

```ruby
CacheBox.new.with { sleep 4; 42 }
```

## Release

Generate and save the https://rubygems.org token in your envchain:

```sh
envchain -s -n rubygems GEM_HOST_API_KEY
```

Publish the new gem:

```sh
envchain rubygems gem push release/cache_box.gem
```
