# CacheBox

An ETL pipeline library with implicit caching.

Store single payloads for easy reuse, chain processing results for quick
development and iteration, architect complex excution graphs with automatic
concurrency, basically do whatever you want! It's small, lightweight, and has
_ZERO_ runtime dependencies, suitable for building everything from quick one-off
scripts, lambdas, and long running DAG workflows.

## Status

W.I.P and unstable API (preview gems available on
https://rubygems.org/gems/cache_box)

Supported ruby versions:
* Ruby `3.1` and `3.0`: Experimenta support, Ractors needs to stabilise before I
  release a stable version.
* Ruby `2.7` and `2.6`: Not supported since minitest is broken for those
  versions, also they don't have Ractors so they will be irrelevant anyway.
* Other ruby flavors: No, only `MRI` is officially supported.

## Example

Here is a toy script that showcases extracting webserver log analytics:
- [examples/webserver-logs](./examples/webserver-logs)

## Install

```sh
gem install cache_box
```

## Usage

### Basic

Run it once to execute the block and cache the result:

```ruby
CacheBox.new.with { puts 'I am running!'; 42 }
# I am running!
# => 42
```

Run it again to return the result from cache instead of executing the block:

```ruby
CacheBox.new.with { puts 'I am running!'; raise 'Oopsie!' }
# => 42
```

Cache is stored by default in `./.cache/namespace/name`, but you can change the
namespace and cache name:

```ruby
CacheBox.new(:other_namespace).with(:other_name) { 42 }
```

Reuse the cache object with different names. Notice the block is not executed
the second time you run it with the same name.

```ruby
cache = CacheBox.new

cache.with(:one) { 42 }
# => 42

cache.with(:two) { 10 }
# => 10

cache.with(:one) { raise 'I am an unexpected error!' }
# => 42
```

### Chain

Use `CacheBox::Chain` class for a linear workflow:

```ruby
chain = CacheBox::Chain.new(:workflow)

chain.add(:first)  {       puts 'First!';  1             }
chain.add(:second) { |box| puts 'Second!'; box.input + 2 }
chain.add(:third)  { |box| puts 'Third';   box.input + 3 }

chain.run!
# First!
# Second!
# Third!
# => 6
```

The second time you execute `chain.run!` it will take the result from cache
instead of re-running the chain. It will check in reverse order to see if a
cached intermediary value is available, so if you exipre the cache on `:second`
but the cache on `:third` still has a value, it will not run `:second` again.

### Graph

Need even more power? Try using `CacheBox::Graph` for a `D.A.G.` style workflow:

```ruby
graph = CacheBox::Graph.new(:dag)

graph.node(:first)  { puts 'First!';  1 }
graph.node(:second) { puts 'Second!'; 2 }

graph.node(third: [:first, :second]) do |box|
  puts 'Third'

  box.input[:first] + box.input[:second] + 3
end

graph.run!
# First!
# Second!
# Third!
# => { third: 6 }
```

You can give it a different scheduler if you want it to run concurrently:

```ruby
scheduler = CacheBox::Scheduler::Concurrent.new
graph     = CacheBox::Graph.new(:DAG, scheduler: scheduler)

graph.node(:first)  { puts 'First!';  1 }
graph.node(:second) { puts 'Second!'; 2 }

graph.node(third: [:first, :second]) do |box|
  puts 'Third'

  box.input[:first] + box.input[:second] + 3
end

graph.run!
# Second!
# First!
# Third!
# => { third: 6 }
```

The `:first` and `:second` nodes will run concurrently, when both finish
`:third` will be scheduled.

### Expiry

To expire the cache run:

```ruby
cache.expire!
```

Or with arguments to expire specific caches:

```ruby
cache.expire!(:first, :second)
```

## Development

Instructions can be found in [./documentation/development.md](./documentation/development.md)


## License

The rest of the code is covered by the BSD 3-clause License, see
[LICENSE](LICENSE) for more details.
