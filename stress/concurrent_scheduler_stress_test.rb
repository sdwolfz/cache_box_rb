# frozen_string_literal: true

require_relative 'stress_helper'

checks = proc do |storage|
  subject = CacheBox::Scheduler::Concurrent.new

  subject.add(:a)
  subject.add(:b)
  subject.add(:c)
  subject.add(:d)
  subject.add(:e)
  subject.add(:f)
  subject.add(:g)
  subject.add(h: :a)
  subject.add(i: [:b, :c])
  subject.add(j: [:d, :e, :f])
  subject.add(k: [:a, :h, :i])
  subject.add(l: [:j, :k])

  effect = [[], [], [], []]
  lock   = Mutex.new

  unit  = CacheBox::Unit.new(:namespace, storage: storage)
  nodes = {
    'a' => ['a', proc { unit.with(:a) { lock.synchronize { effect[0] << 1;  1  } } }],
    'b' => ['b', proc { unit.with(:b) { lock.synchronize { effect[0] << 2;  2  } } }],
    'c' => ['c', proc { unit.with(:c) { lock.synchronize { effect[0] << 3;  3  } } }],
    'd' => ['d', proc { unit.with(:d) { lock.synchronize { effect[0] << 4;  4  } } }],
    'e' => ['e', proc { unit.with(:e) { lock.synchronize { effect[0] << 5;  5  } } }],
    'f' => ['f', proc { unit.with(:f) { lock.synchronize { effect[0] << 6;  6  } } }],
    'g' => ['g', proc { unit.with(:g) { lock.synchronize { effect[0] << 7;  7  } } }],
    'h' => ['h', proc { unit.with(:h) { lock.synchronize { effect[1] << 8;  8  } } }],
    'i' => ['i', proc { unit.with(:i) { lock.synchronize { effect[1] << 9;  9  } } }],
    'j' => ['j', proc { unit.with(:j) { lock.synchronize { effect[1] << 10; 10 } } }],
    'k' => ['k', proc { unit.with(:k) { lock.synchronize { effect[2] << 11; 11 } } }],
    'l' => ['l', proc { unit.with(:l) { lock.synchronize { effect[3] << 12; 12 } } }]
  }

  result   = subject.run!(nodes, unit)
  expected = { g: 7, l: 12 }
  match    = expected == result

  unless match
    puts 'Error: Result did not match expectation!'
    puts "Expected: #{expected.inspect}"
    puts "Got:      #{result.inspect}"

    raise 'Stress test error!'
  end

  result   = effect.map(&:sort)
  expected = [[1, 2, 3, 4, 5, 6, 7], [8, 9, 10], [11], [12]]
  match    = expected == result

  unless match
    puts 'Error: Result did not match expectation!'
    puts "Expected: #{expected.inspect}"
    puts "Got:      #{result.inspect}"
    puts "Original: #{effect.inspect}"

    raise 'Stress test error!'
  end
end

# -----------------------------------------------------------------------------

puts "Running: #{__FILE__}"

total = ENV.fetch('TOTAL', '1_000_000').to_i
total.times do |index|
  if index > 0 && index % 1000 == 0
    puts "#{index} / #{total} (#{(index.to_f / total * 100).round(1)}%)"
  end

  memory_storage = CacheBox::Storage::Memory.new
  checks.call(memory_storage)
end
